# Template pycap game v1.0
# Written by Farbs

#appIni = {	"mCompanyName"		: "CompanyNameGoesHere",
#		"mFullCompanyName"	: "CompanyNameGoesHere",
#		"mProdName"		: "Pycap Template Game",
#		"mProductVersion"	: "1.0",
#		"mTitle"			: "Pycap Template Game v1.0",
#		"mRegKey"			: "PycapTest",
#		"mWidth"			: 800,
#		"mHeight"			: 600,
#		"mAutoEnable3D"	: 1,
#		"mVSyncUpdates"	: 1 }

# This dict provides the basic information required to initialize the application.
# Filling it out is a great way to pretend that you're making progress on a new project.
appIni = {  "mCompanyName"         : "NetCharm",
            "mFullCompanyName"     : "NetCharm Studio!",
            "mProdName"            : "PyCap Test Game",
            "mProductVersion"      : "1.0",
            "mTitle"               : "PyCap Test Game v1.0",
            "mRegKey"              : "PyCapTest",
            "mWidth"               : 800,
            "mHeight"              : 600,
            "mAutoEnable3D"        : 1,
            "mVSyncUpdates"        : 1,
            "mWantFMod"            : 0,
            "mNoSoundNeeded"       : 0,
            "mRecordingDemoBuffer" : 1,
            "mReadFromRegistry"    : 1,
            "mDebugKeysEnabled"    : 1 }

import PyCap as PC
PCR = None

def loadBase():
	import PyCapRes
	global PCR
	PCR = PyCapRes

def init():
	pass

def update( delta ):
	PC.markDirty()

def draw():
	pass
