//--------------------------------------------------
// main.cpp
//
// Main function for Pycap
// All this does is init and start the application
//
//--------------------------------------------------

#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.VC90.CRT' version='9.0.30729.1' processorArchitecture='X86' publicKeyToken='1fc8b3b9a1e18e3b' language='*'\"")

// includes
#include "PycapApp.h"

// namespace (I'll keep using this... avoids collisions with other libs etc)
using namespace Sexy;

// main function
int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
{
	// grab the handle (framework requires this)
	gHInstance = hInstance;

	// create game application
	PycapApp* app = new PycapApp();

	// init the application
	app->Init();

	// start the application
	app->Start();

	// clean up
	delete app;

	// exit, no errors
	return 0;
}