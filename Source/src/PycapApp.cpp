//--------------------------------------------------
// PycapApp
//
// Pycap application object
//
//--------------------------------------------------

// includes
#include "PycapApp.h"
#include "PycapBoard.h"

#include "WidgetManager.h"
#include "Graphics.h"
#include "Font.h"
#include "SoundManager.h"
#include "SoundInstance.h"

#ifndef INITGUID
#define INITGUID
#endif
#include <Dmusicc.h>
#include <Dmusici.h>

#include <Python.h>

// namespace
using namespace Sexy;



// static data definition
PycapApp* PycapApp::sApp = NULL;


// functions

//--------------------------------------------------
// PycapApp
//--------------------------------------------------
PycapApp::PycapApp()
{
	// Set up python
	Py_Initialize();

	// Redirect stdout and stderr to files (since we can't seem to use console output)
	PyRun_SimpleString("import sys");
	PyRun_SimpleString( ( "sys.stdout = open( \"" + GetAppDataFolder() + "out.txt\", 'w' )" ).c_str() );
	PyRun_SimpleString( ( "sys.stderr = open( \"" + GetAppDataFolder() + "err.txt\", 'w' )" ).c_str() );

	// Set up Pycap module
	static PyMethodDef resMethods[]	= {
		{"markDirty", pMarkDirty, METH_VARARGS, "markDirty()\nMark the screen dirty & call a refresh."},
		{"fillRect", pFillRect, METH_VARARGS, "fillRect( x, y, width, height )\nFill a specified rect with the current colour."},
		{"setColour", pSetColour, METH_VARARGS, "setColour( red, green, blue, alpha )\nSet the draw colour. Use a value between 0 and 255 for each component."},
		{"setFont", pSetFont, METH_VARARGS, "setFont( font )\nSet the active font."},
		{"setColourize", pSetColourize, METH_VARARGS, "setColourize( on )\nEnable/Disable colourized drawing."},
		{"drawImage", pDrawImage, METH_VARARGS, "drawImage( image, x, y )\nDraw an image resource at pixel coords."},
		{"drawImageF", pDrawImageF, METH_VARARGS, "drawImageF( image, fx, fy )\nDraw an image resource at float coords."},
		{"drawImageRot", pDrawImageRot, METH_VARARGS, "drawImageRot( image, x, y, angle )\nDraw an image resource at pixel coords rotated by a given angle."},
		{"drawImageRotF", pDrawImageRotF, METH_VARARGS, "drawImageRot( image, x, y, angle )\nDraw an image resource at float coords rotated by a given angle."},
		{"drawImageScaled", pDrawImageScaled, METH_VARARGS, "drawImageScaled( image, x, y, width, height )\nScale and draw an image resource at int coords."},
		{"drawString", pDrawString, METH_VARARGS, "drawString( string, x, y )\nWrite a given string to the screen using the current font."},
		{"showMouse", pShowMouse, METH_VARARGS, "showMouse( show )\nShow or hide the mouse cursor."},
		{"drawmodeNormal", pDrawmodeNormal, METH_VARARGS, "drawmodeNormal()\nSet the drawing mode to normal."},
		{"drawmodeAdd", pDrawmodeAdd, METH_VARARGS, "drawmodeAdd()\nSet the drawing mode to additive."},
		{"playSound", pPlaySound, METH_VARARGS, "playSound( sound, volume, panning, pitch )\nPlay a sound, providing id, and optionally volume, panning, and pitch adjust."},
		{"stopSound", pStopSound, METH_VARARGS, "stopSound( sound )\nStop a playing sound, providing id,"},
		{"setSoundVolume", pSetSoundVolume, METH_VARARGS, "playSound( sound, volume, panning, pitch )\nChange a playing sound, providing id, and optionally volume, panning, and pitch adjust."},
		{"readReg", pReadReg, METH_VARARGS, "readReg( key )\nRead an entry from the system registry."},
		{"writeReg", pWriteReg, METH_VARARGS, "writeReg( key, data )\nWrite an entry to the system registry."},
		{"playTune", pPlayTune, METH_VARARGS, "playTune( tune, loopCount )\nPlay a midi tune, with optional loop parameter."},
		{"stopTune", pStopTune, METH_VARARGS, "stopTune( tune )\nStop playing a midi tune. If not specified, all tunes are stopped."},
		{"setTuneVolume", pSetTuneVolume, METH_VARARGS, "setTuneVolume( volume )\nChange the global volume for all midi"},
		{"playMusic", pPlayMusic, METH_VARARGS, "playMusic( music, loopCount )\nPlay a music, with optional loop parameter."},
		{"stopMusic", pStopMusic, METH_VARARGS, "stopMusic( music )\nStop playing a music. If not specified, all musics are stopped."},
		{"setMusicVolume", pSetMusicVolume, METH_VARARGS, "setMusicVolume( music, volume )\nChange the global volume for all music"},
		{"setClipRect", pSetClipRect, METH_VARARGS, "setClipRect( x, y, width, height )\nSet the clipping rectangle."},
		{"clearClipRect", pClearClipRect, METH_VARARGS, "clearClipRect()\nClear the clipping rectangle."},
		{"setTranslation", pSetTranslation, METH_VARARGS, "setTranslation( x, y )\nSet the translation applied to all draw calls."},
		{"setFullscreen", pSetFullscreen, METH_VARARGS, "setFullscreen( fullscreen )\nSet whether the app should be in fullscreen or windowed mode."},
		{"getFullscreen", pGetFullscreen, METH_VARARGS, "getFullscreen()\nGet whether the app is in fullscreen or windowed mode."},
		{"allowAllAccess", pAllowAllAccess, METH_VARARGS, "allowAllAccess( fileName )\nTell the OS that all users can view and modify a file. Required for Vista."},
		{"getAppDataFolder", pGetAppDataFolder, METH_VARARGS, "getAppDataFolder()\nGet the folder that game data should be saved to. Required for Vista."},
		{"Pause", pPause, METH_VARARGS, "Pause( pause )\nPause App."},
		{NULL, NULL, 0, NULL}
	};

	PyObject* mod = Py_InitModule("PyCap", resMethods);
	// general error location warning
	if (PyErr_Occurred())
	{
		PyErr_Print();
		Popup( StrFormat( "Some kind of python error occurred in PycapApp(), while importing Pycap module." ) );
	}

  //Py_INCREF(&PyCapWidgetType);
  //PyModule_AddObject(mod, "Widget", (PyObject *)&PyCapWidgetType);

	// Open game module
	PyObject *pName;
  pName = PyString_FromString( "game" );
  pModule = PyImport_Import(pName);
  Py_DECREF(pName);
  if (pModule == NULL)
	{
		Popup( StrFormat( "Failed to load game.py" ) );
		PyErr_Print();
		return; // we're screwed.
	}
  pDict = PyModule_GetDict(pModule); // grab namespace dictionary

	//-------------------
	// Initialize members

	// inherited
	// read inherited members from python dictionary
	PyObject *iniDict;
	iniDict = PyDict_GetItemString( pDict, "appIni" );
	if ( iniDict && PyDict_Check( iniDict ) )
	{
		PyObject *inObject;

		inObject = PyDict_GetItemString( iniDict, "mCompanyName" );
		if ( inObject && PyString_Check( inObject ) )
		{
			mCompanyName = PyString_AsString( inObject );
		}
		else
		{
			Popup( "appIni doesn't specify mCompanyName correctly" );
			PyErr_Print();
			return;
		}

		inObject = PyDict_GetItemString( iniDict, "mFullCompanyName" );
		if ( inObject && PyString_Check( inObject ) )
		{
			mFullCompanyName = PyString_AsString( inObject );
		}
		else
		{
			Popup( "appIni doesn't specify mFullCompanyName correctly" );
			PyErr_Print();
			return;
		}

		inObject = PyDict_GetItemString( iniDict, "mProdName" );
		if ( inObject && PyString_Check( inObject ) )
		{
			mProdName = PyString_AsString( inObject );
		}
		else
		{
			Popup( "appIni doesn't specify mProdName correctly" );
			PyErr_Print();
			return;
		}

		inObject = PyDict_GetItemString( iniDict, "mProductVersion" );
		if ( inObject && PyString_Check( inObject ) )
		{
			mProductVersion = PyString_AsString( inObject );
		}
		else
		{
			Popup( "appIni doesn't specify mProductVersion correctly" );
			PyErr_Print();
			return;
		}

		inObject = PyDict_GetItemString( iniDict, "mTitle" );
		if ( inObject && PyString_Check( inObject ) )
		{
			mTitle = PyString_AsString( inObject );
		}
		else
		{
			Popup( "appIni doesn't specify mTitle correctly" );
			PyErr_Print();
			return;
		}

		inObject = PyDict_GetItemString( iniDict, "mRegKey" );
		if ( inObject && PyString_Check( inObject ) )
		{
			mRegKey = PyString_AsString( inObject );
		}
		else
		{
			Popup( "appIni doesn't specify mRegKey correctly" );
			PyErr_Print();
			return;
		}

		inObject = PyDict_GetItemString( iniDict, "mWidth" );
		if ( inObject && PyInt_Check( inObject ) )
		{
			mWidth = PyInt_AsLong( inObject );
		}
		else
		{
            mWidth = 800;
			//Popup( "appIni doesn't specify mWidth correctly" );
            Log( "appIni doesn't specify mWidth correctly" );
			PyErr_Print();
			//return;
		}

		inObject = PyDict_GetItemString( iniDict, "mHeight" );
		if ( inObject && PyInt_Check( inObject ) )
		{
			mHeight = PyInt_AsLong( inObject );
		}
		else
		{
            mHeight = 600;
			//Popup( "appIni doesn't specify mHeight correctly" );
            Log( "appIni doesn't specify mHeight correctly" );
			PyErr_Print();
			//return;
		}

		inObject = PyDict_GetItemString( iniDict, "mAutoEnable3D" );
		if ( inObject && PyInt_Check( inObject ) )
		{
			mAutoEnable3D = PyInt_AsLong( inObject ) == 1;
		}
		else
		{
            mAutoEnable3D = true;
			//Popup( "appIni doesn't specify mAutoEnable3D correctly" );
            Log( "appIni doesn't specify mAutoEnable3D correctly" );
			PyErr_Print();
			//return;
		}

		inObject = PyDict_GetItemString( iniDict, "mVSyncUpdates" );
		if ( inObject && PyInt_Check( inObject ) )
		{
			mVSyncUpdates = PyInt_AsLong( inObject ) == 1;
		}
		else
		{
            mVSyncUpdates = true;
			//Popup( "appIni doesn't specify mVSyncUpdates correctly" );
            Log( "appIni doesn't specify mVSyncUpdates correctly" );
			PyErr_Print();
			//return;
		}

	    inObject = PyDict_GetItemString( iniDict, "mWantFMod" );
	    if ( inObject && PyInt_Check( inObject ) )
	    {
		    mWantFMod = PyInt_AsLong( inObject ) == 1;
	    }
	    else
	    {
            mWantFMod = false;
		    //Popup( "appIni doesn't specify mWantFMod correctly" );
            Log("appIni doesn't specify mWantFMod correctly");
		    PyErr_Print();
		    //return;
	    }

	    inObject = PyDict_GetItemString( iniDict, "mNoSoundNeeded" );
	    if ( inObject && PyInt_Check( inObject ) )
	    {
		    mNoSoundNeeded = PyInt_AsLong( inObject ) == 1;
	    }
	    else
	    {
            mNoSoundNeeded = false;
		    //Popup( "appIni doesn't specify mNoSoundNeeded correctly" );
            Log("appIni doesn't specify mNoSoundNeeded correctly");
		    PyErr_Print();
		    //return;
	    }

		inObject = PyDict_GetItemString( iniDict, "mReadFromRegistry" );
		if ( inObject && PyInt_Check( inObject ) )
		{
			mReadFromRegistry = PyInt_AsLong( inObject ) == 1;
		}
		else
		{
            mReadFromRegistry = false;
			//Popup( "appIni doesn't specify mReadFromRegistry correctly" );
            Log( "appIni doesn't specify mReadFromRegistry correctly" );
			PyErr_Print();
			//return;
		}

		inObject = PyDict_GetItemString( iniDict, "mDebugKeysEnabled" );
		if ( inObject && PyInt_Check( inObject ) )
		{
			mDebugKeysEnabled = PyInt_AsLong( inObject ) == 1;
		}
		else
		{
            mDebugKeysEnabled = false;
			//Popup( "appIni doesn't specify mDebugKeysEnabled correctly" );
            Log( "appIni doesn't specify mDebugKeysEnabled correctly" );
			PyErr_Print();
			//return;
		}

		inObject = PyDict_GetItemString( iniDict, "mRecordingDemoBuffer" );
		if ( inObject && PyInt_Check( inObject ) )
		{
			mRecordingDemoBuffer = PyInt_AsLong( inObject ) == 1;
		}
		else
		{
            mRecordingDemoBuffer = false;
			//Popup( "appIni doesn't specify mRecordingDemoBuffer correctly" );
            Log( "appIni doesn't specify mRecordingDemoBuffer correctly" );
			PyErr_Print();
			//return;
		}

	}
	else
	{
		Popup( "appIni object is missing or not a dict" );
		PyErr_Print();
		return;
	}

	// own members
	sApp				= this;
	mBoard				= NULL;
	mResources			= NULL;
	mResFailed			= false;
	mMidiInitialized	= false;
	mDMPerformance		= NULL;

	//mLoadingScreen      = NULL;
	//-------------------
}

//--------------------------------------------------
// ~PycapApp
//--------------------------------------------------
PycapApp::~PycapApp()
{

	//delete mLoadingScreen;

	// clean up board if necessary
	if( mBoard != NULL )
	{
		mWidgetManager->RemoveWidget( mBoard );
		delete mBoard;
	}

	// clean up resources object
	if( mResources != NULL )
	{
		delete mResources;
	}

	// clean up app pointer if it's pointing to this
	if( sApp == this )
	{
		sApp = NULL;
	}

	// clean up python
    Py_DECREF(pModule);	// drop the module
    Py_Finalize();		// shut down the interpreter

	// clean up direct music
    // Release the segment.
    //g_pMIDISeg->Release();
    // clean up the performance object.
	// if initialized
	if( mMidiInitialized )
	{
		mDMPerformance->CloseDown();
		mDMPerformance->Release();
	}
    // clean up the loader object.
    //g_pLoader->Release();
    // release COM.
    CoUninitialize();
}

void PycapApp::Log(const std::string& theString)
{
    PyRun_SimpleString( ( "print( \"" + theString + "\" )" ).c_str() );

    //LPDIRECTDRAWSURFACE lpDDS;
    //HDC hdc;
    //COLORREF bcolor;
    //COLORREF tcolor;
    //int posx;
    //int posy;

    //if(SUCCEEDED(lpDDS->GetDC(&hdc)))
    //{
    //  SetBkColor(hdc, bcolor);
    //  SetTextColor(hdc, tcolor);
    //  TextOut(hdc, posx, posy, theString.c_str(), lstrlen(theString.c_str()));
    //  lpDDS->ReleaseDC(hdc);
    //}


}

void PycapApp::Log(const std::wstring& theString)
{
    PyRun_SimpleString( ( "print( u\"" + WStringToString(theString) + "\" )" ).c_str() );
}

//--------------------------------------------------
// Init
//--------------------------------------------------
void PycapApp::Init()
{
    SetAppDataFolder(".\\_AppData\\");

    //sApp->mWantFMod = true;
	// call parent
	SexyAppBase::Init();

    //MessageBoxA(NULL, GetAppDataFolder().c_str(),"Info", MB_OK);
    //SetAppDataFolder(GetAppDataFolder());
    //SetAppDataFolder(".\\_AppData\\");
    //MessageBoxA(NULL, GetAppDataFolder().c_str(),"Info", MB_OK);

	// Init DirectMusic
	if ( FAILED( CoInitialize( NULL ) ) )
	{
		return;
	}

    if ( FAILED( CoCreateInstance(	CLSID_DirectMusicPerformance,
									NULL,
									CLSCTX_INPROC,
									IID_IDirectMusicPerformance2,
									(void**)&mDMPerformance ) ) )
	{
		return;
	}
	if ( FAILED( mDMPerformance->Init( NULL, NULL, NULL ) ) )
	{
		return;
	}
	if ( FAILED( mDMPerformance->AddPort(NULL) ) )
	{
		return;
	}

	mMidiInitialized = true;

	//mLoadingScreen=new LoadingScreen(this);
	//mWidgetManager->AddWidget(mLoadingScreen);
}

//--------------------------------------------------
// LoadingThreadProc
//--------------------------------------------------
void PycapApp::LoadingThreadProc()
{
	// call parent (empty at the moment, but a good habit)
	SexyAppBase::LoadingThreadProc();

	// create the res object
	mResources = new PycapResources();
}

//--------------------------------------------------
// LoadingThreadCompleted
//--------------------------------------------------
void PycapApp::LoadingThreadCompleted()
{
	// call parent
	SexyAppBase::LoadingThreadCompleted();

	//mWidgetManager->RemoveWidget(mLoadingScreen);
	//delete mLoadingScreen;
	//mLoadingScreen = NULL;

	// check for a failed resource load (not using mLoadingFailed as this terminates the app badly)
	if( mResFailed )
	{
		// Nothing much happens if we return before adding the board... just a black screen
		// Error message widget should be added here
		Popup("The game did not load properly. Sorry, but it's not going to work.");
		return;
	}

	// create the initial board object
	PycapBoard* newBoard = new PycapBoard();

	// remove current board if appropriate
	if( mBoard != NULL )
	{
		mWidgetManager->RemoveWidget( mBoard );
	}

	// store link
	mBoard = newBoard;

	// resize the board to the application size
	newBoard->Resize( 0, 0, PycapApp::sApp->mWidth, PycapApp::sApp->mHeight );

	// add the board widget
	mWidgetManager->AddWidget( newBoard );

	// give the board focus
	mWidgetManager->SetFocus( newBoard );

	// mMainWidget=new MainWidget(this);
	// mWidgetManager->AddWidget(mMainWidget);
	// mWidgetManager->SetFocus(mMainWidget);
}

//--------------------------------------------------
// GotFocus
//--------------------------------------------------
void PycapApp::GotFocus()
{
	PyObject* pGFFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "gotFocus" );

    if ( pGFFunc && PyCallable_Check( pGFFunc ) )
	{
        PyObject_CallObject( pGFFunc, NULL );
    }
}

//--------------------------------------------------
// LostFocus
//--------------------------------------------------
void PycapApp::LostFocus()
{
	PyObject* pLFFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "lostFocus" );

    if ( pLFFunc && PyCallable_Check( pLFFunc ) )
	{
        PyObject_CallObject( pLFFunc, NULL );
    }
}

//--------------------------------------------------
// SwitchScreenMode
//--------------------------------------------------
void PycapApp::SwitchScreenMode( bool wantWindowed, bool is3d )
{
	// Super
	SexyAppBase::SwitchScreenMode( wantWindowed, is3d );

	// attempt to call python fullscreen or windowed notifier
	// failed attempts still notify, but notify correctly
	if ( mIsWindowed )
	{
		// windowed
		PyObject* pWFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "onWindowed" );
		if ( pWFunc && PyCallable_Check( pWFunc ) )
		{
			PyObject_CallObject( pWFunc, NULL );
		}
	}
	else
	{
		// fullscreen
		PyObject* pFSFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "onFullscreen" );
		if ( pFSFunc && PyCallable_Check( pFSFunc ) )
		{
			PyObject_CallObject( pFSFunc, NULL );
		}
	}
}


//--------------------------------------------------
// pMarkDirty
//--------------------------------------------------
PyObject* PycapApp::pMarkDirty( PyObject* self, PyObject* args )
{
	// parse the arguments
	//if( !PyArg_ParseTuple( args, NULL ) )
    //   return NULL;

	// mark the board as dirty
	sApp->mBoard->MarkDirty();

	// return, 'cos we're done
	Py_INCREF( Py_None );
    return Py_None;
}

//--------------------------------------------------
// pSetColour
//--------------------------------------------------
PyObject* PycapApp::pSetColour( PyObject* self, PyObject* args )
{
	// parse the arguments
	int r, g, b, a;
	if( !PyArg_ParseTuple( args, "iiii", &r, &g, &b, &a ) )
        return NULL;

	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "SetColour() failed: Not currently drawing!" ) );
		return NULL;
	}

	// create new colour object, set colour
	graphics->SetColor( Color( r, g, b, a ) );

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pSetFont
//--------------------------------------------------
PyObject* PycapApp::pSetFont( PyObject* self, PyObject* args )
{
	// parse the arguments
	int i;
	if( !PyArg_ParseTuple( args, "i", &i ) )
        return NULL;

	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "SetFont() failed: Not currently drawing!" ) );
		return NULL;
	}

	// get the font
	Font* font = sApp->mResources->getFont( i );
	if( !font )
	{
		// throw an exception
		PyErr_SetString( PyExc_IOError, "Failed to reference font." );

		// exit, returning None/NULL
		return NULL;
	}

	// set the active font
	graphics->SetFont( font );

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pSetColourize
//--------------------------------------------------
PyObject* PycapApp::pSetColourize( PyObject* self, PyObject* args )
{
	// parse the arguments
	int colourize;
	if( !PyArg_ParseTuple( args, "i", &colourize ) )
        return NULL;

	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "SetColourize() failed: Not currently drawing!" ) );
		return NULL;
	}

	// create new colour object, set colour
	graphics->SetColorizeImages( colourize != 0 );

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pFillRect
//--------------------------------------------------
PyObject* PycapApp::pFillRect( PyObject* self, PyObject* args )
{
	// parse the arguments
	int x, y, w, h;
	if( !PyArg_ParseTuple( args, "iiii", &x, &y, &w, &h ) )
        return NULL;

	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "FillRect() failed: Not currently drawing!" ) );
		return NULL;
	}

	// create new colour object, set colour
	graphics->FillRect( x, y, w, h );

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pDrawImage
//--------------------------------------------------
PyObject* PycapApp::pDrawImage( PyObject* self, PyObject* args )
{
	// parse the arguments
	int i, x, y;
	if( !PyArg_ParseTuple( args, "iii", &i, &x, &y ) )
        return NULL;

	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "DrawImage() failed: Not currently drawing!" ) );
		return NULL;
	}

	// get the image
	Image* image = sApp->mResources->getImage( i );
	if( !image )
	{
		// throw an exception
		PyErr_SetString( PyExc_IOError, "Failed to reference image." );

		// exit, returning None/NULL
		return NULL;
	}

	// perform the blit
	graphics->DrawImage( image, x, y );

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pDrawImageF
//--------------------------------------------------
PyObject* PycapApp::pDrawImageF( PyObject* self, PyObject* args )
{
	// parse the arguments
	int i;
	float x, y;
	if( !PyArg_ParseTuple( args, "iff", &i, &x, &y ) )
        return NULL;

	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "DrawImageF() failed: Not currently drawing!" ) );
		return NULL;
	}

	// get the image
	Image* image = sApp->mResources->getImage( i );
	if( !image )
	{
		// throw an exception
		PyErr_SetString( PyExc_IOError, "Failed to reference image." );

		// exit, returning None/NULL
		return NULL;
	}

	// perform the blit
	graphics->DrawImageF( image, x, y );

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pDrawImageRot
//--------------------------------------------------
PyObject* PycapApp::pDrawImageRot( PyObject* self, PyObject* args )
{
	// parse the arguments
	int i;
	float x, y, r;
	if( !PyArg_ParseTuple( args, "ifff", &i, &x, &y, &r ) )
        return NULL;

	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "DrawImageRot() failed: Not currently drawing!" ) );
		return NULL;
	}

	// get the image
	Image* image = sApp->mResources->getImage( i );
	if( !image )
	{
		// throw an exception
		PyErr_SetString( PyExc_IOError, "Failed to reference image." );

		// exit, returning None/NULL
		return NULL;
	}

	// perform the blit
	graphics->DrawImageRotated( image, x, y, r );

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pDrawImageRotF
//--------------------------------------------------
PyObject* PycapApp::pDrawImageRotF( PyObject* self, PyObject* args )
{
	// parse the arguments
	int i;
	float x, y, r;
	if( !PyArg_ParseTuple( args, "ifff", &i, &x, &y, &r ) )
        return NULL;

	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "DrawImageRotF() failed: Not currently drawing!" ) );
		return NULL;
	}

	// get the image
	Image* image = sApp->mResources->getImage( i );
	if( !image )
	{
		// throw an exception
		PyErr_SetString( PyExc_IOError, "Failed to reference image." );

		// exit, returning None/NULL
		return NULL;
	}

	// perform the blit
	graphics->DrawImageRotatedF( image, x, y, r );

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pDrawImageScaled
//--------------------------------------------------
PyObject* PycapApp::pDrawImageScaled( PyObject* self, PyObject* args )
{
	// parse the arguments
	int i;
	float x, y, w, h;
	if( !PyArg_ParseTuple( args, "iffff", &i, &x, &y, &w, &h ) )
        return NULL;

	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "DrawImageScaled() failed: Not currently drawing!" ) );
		return NULL;
	}

	// get the image
	Image* image = sApp->mResources->getImage( i );
	if( !image )
	{
		// throw an exception
		PyErr_SetString( PyExc_IOError, "Failed to reference image." );

		// exit, returning None/NULL
		return NULL;
	}

	// perform the blit
	graphics->DrawImage( image, (int)x, (int)y, (int)w, (int)h );

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pDrawString
//--------------------------------------------------
PyObject* PycapApp::pDrawString( PyObject* self, PyObject* args )
{
	// parse the arguments
	char* string;
	float x, y;
	if( !PyArg_ParseTuple( args, "sff", &string, &x, &y ) )
        return NULL;

	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "DrawString() failed: Not currently drawing!" ) );
		return NULL;
	}

	// perform the blit
	graphics->DrawString( string, (int)x, (int)y );

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pShowMouse
//--------------------------------------------------
PyObject* PycapApp::pShowMouse( PyObject* self, PyObject* args )
{
	// parse the arguments
	int show;
	if( !PyArg_ParseTuple( args, "i", &show ) )
        return NULL;

	// test argument
	if( show )
	{
		sApp->SetCursor( CURSOR_POINTER );
	}
	else
	{
		sApp->SetCursor( CURSOR_NONE );
	}

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pDrawmodeNormal
//--------------------------------------------------
PyObject* PycapApp::pDrawmodeNormal( PyObject* self, PyObject* args )
{
	// parse the arguments
	//if( !PyArg_ParseTuple( args, NULL ) )
    //   return NULL;

	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "DrawmodeNormal() failed: Not currently drawing!" ) );
		return NULL;
	}

	// set the draw mode
	graphics->SetDrawMode( Graphics::DRAWMODE_NORMAL );

	// return, 'cos we're done
	Py_INCREF( Py_None );
    return Py_None;
}

//--------------------------------------------------
// pDrawmodeAdd
//--------------------------------------------------
PyObject* PycapApp::pDrawmodeAdd( PyObject* self, PyObject* args )
{
	// parse the arguments
	//if( !PyArg_ParseTuple( args, NULL ) )
    //   return NULL;

	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "DrawmodeAdd() failed: Not currently drawing!" ) );
		return NULL;
	}

	// set the draw mode
	graphics->SetDrawMode( Graphics::DRAWMODE_ADDITIVE );

	// return, 'cos we're done
	Py_INCREF( Py_None );
    return Py_None;
}

//--------------------------------------------------
// pPlaySound
//--------------------------------------------------
PyObject* PycapApp::pPlaySound( PyObject* self, PyObject* args )
{
	// parse the arguments
	// required
	int index;
	// optional
	float	volume = 1.0f;
	float	panning = 0.0f;
	float	pitchAdjust = 0.0f;
	int loop = 0;
	if( !PyArg_ParseTuple( args, "i|fffi", &index, &volume, &panning, &pitchAdjust, &loop) )
        return NULL;

	// check that the sound exists
	if( !sApp->mResources->soundExists( index ) )
	{
		// throw an exception
		PyErr_SetString( PyExc_IOError, "Failed to reference sound." );

		// exit, returning None/NULL
		return NULL;
	}

	// set the sound parameters
	SoundInstance* sound = sApp->mSoundManager->GetSoundInstance( index );
	if( sound )
	{
		sound->SetVolume( volume );
		sound->SetPan( int( panning * 1000.0f ) );
		sound->AdjustPitch( pitchAdjust );

		// play the sound
		if(loop==0)
		{
		  sound->Play( false, true );	// Play sound. Always auto-release the instance.
		}
		else
		{
		  sound->Play( true, true );	// Play sound looping. Always auto-release the instance.
		}
	}

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pStopSound
//--------------------------------------------------
PyObject* PycapApp::pStopSound( PyObject* self, PyObject* args )
{
	// parse the arguments
	int i = -1;
	if( !PyArg_ParseTuple( args, "|i", &i ) )
        return NULL;

	// if initialized
	if( sApp->mMidiInitialized )
	{

		if( i != -1 )
		{

		}

	}

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pSetSoundVolume
//--------------------------------------------------
PyObject* PycapApp::pSetSoundVolume( PyObject* self, PyObject* args )
{
	// parse the arguments
	float l = 0.85f;
	if( !PyArg_ParseTuple( args, "f", &l ) )
        return NULL;

	// if initialized
	if( sApp->mSoundManager )
	{
        sApp->SetSfxVolume(l);
	}

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pReadReg
//--------------------------------------------------
PyObject* PycapApp::pReadReg( PyObject* self, PyObject* args )
{
	// parse the arguments
	char* key;
	if( !PyArg_ParseTuple( args, "s", &key ) )
        return NULL;

	// attempt to read the string
	std::string string;
	if( sApp->RegistryReadString( key, &string ) )
	{
		// return string from registry
		return Py_BuildValue( "s", string.c_str() );
	}
	else
	{
		Py_INCREF( Py_None );
 		return Py_None;
	}
}

//--------------------------------------------------
// pWriteReg
//--------------------------------------------------
PyObject* PycapApp::pWriteReg( PyObject* self, PyObject* args )
{
	// parse the arguments
	char* key;
	char* string;
	if( !PyArg_ParseTuple( args, "ss", &key, &string ) )
        return NULL;

	// attempt to write the string
	// return whether or not we succeeded ('tho I'll probably just ignore it most of the time)
	if( sApp->RegistryWriteString( key, string ) )
	{
		// success
	    return Py_BuildValue( "i", 1 );
	}
	else
	{
		// failure
	    return Py_BuildValue( "i", 0 );
	}
}

//--------------------------------------------------
// pPlayTune
//--------------------------------------------------
PyObject* PycapApp::pPlayTune( PyObject* self, PyObject* args )
{
	// parse the arguments
	int i;
	int repeatCount = 0;
	if( !PyArg_ParseTuple( args, "i|i", &i, &repeatCount ) )
        return NULL;

	// if initialized
	if( sApp->mMidiInitialized )
	{
		// get the segment
		IDirectMusicSegment* tune = sApp->mResources->getTune( i );
		if( !tune )
		{
			// throw an exception
			PyErr_SetString( PyExc_IOError, "Failed to reference tune." );

			// exit, returning None/NULL
			return NULL;
		}

		// set the repeat count (no function for an indefinite loop. Damn!)
		tune->SetRepeats( repeatCount );

		// play the tune
		IDirectMusicSegmentState* segState;
		sApp->mDMPerformance->PlaySegment(tune, 0, 0, &segState);
	}

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pStopTune
//--------------------------------------------------
PyObject* PycapApp::pStopTune( PyObject* self, PyObject* args )
{
	// parse the arguments
	int i = -1;
	if( !PyArg_ParseTuple( args, "|i", &i ) )
        return NULL;

	// if initialized
	if( sApp->mMidiInitialized )
	{
		// default to stopping all segments
		IDirectMusicSegment* tune = NULL;

		// if stopping a particular segment
		if( i != -1 )
		{
			// get the segment
			tune = sApp->mResources->getTune( i );
			if( !tune )
			{
				// throw an exception
				PyErr_SetString( PyExc_IOError, "Failed to reference tune." );

				// exit, returning None/NULL
				return NULL;
			}
		}

		// stop playing
		sApp->mDMPerformance->Stop( tune, NULL, 0, 0 );
	}

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pSetTuneVolume
//--------------------------------------------------
PyObject* PycapApp::pSetTuneVolume( PyObject* self, PyObject* args )
{
	// parse the arguments
	long l;
	if( !PyArg_ParseTuple( args, "l", &l ) )
        return NULL;
	l = -1000000;

	// if initialized
	if( sApp->mMidiInitialized )
	{
		// set volume
		sApp->mDMPerformance->SetGlobalParam( GUID_PerfMasterVolume, &l, sizeof( long ) );
		sApp->mDMPerformance->Invalidate( 0, 0 );
	}

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pPlayMusic
//--------------------------------------------------
PyObject* PycapApp::pPlayMusic( PyObject* self, PyObject* args )
{
	// parse the arguments
	int id = -1;
	int offset = 0;
	int loop = 0;

	if( !PyArg_ParseTuple( args, "i|ii", &id, &offset, &loop ) )
        return NULL;

	// if initialized
	if( sApp->mMusicInterface )
	{
		//sApp->mMusicInterface->PlayMusic(id, 0);

		if(loop==0)
		{
			sApp->mMusicInterface->PlayMusic(id, offset, true);
		}
		else
		{
			sApp->mMusicInterface->PlayMusic(id, offset, false);
		}
	}
	else
	{
		PycapApp::sApp->Popup( "MusicInterface fained!." );
	}

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pStopMusic
//--------------------------------------------------
PyObject* PycapApp::pStopMusic( PyObject* self, PyObject* args )
{
	// parse the arguments
	int i = -1;
	if( !PyArg_ParseTuple( args, "|i", &i ) )
        return NULL;

	// if initialized
	if( sApp->mMusicInterface )
	{
		// if stopping a particular segment
		if( i != -1 )
		{
  			sApp->mMusicInterface->StopMusic(i);
		}
	}

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pSetMusicVolume
//--------------------------------------------------
PyObject* PycapApp::pSetMusicVolume( PyObject* self, PyObject* args )
{
	// parse the arguments
	float l = 1.0f;
	if( !PyArg_ParseTuple( args, "f", &l ) )
        return NULL;

	// if initialized
	if( sApp->mMusicInterface )
	{
		// set volume
		sApp->SetMusicVolume(l);
        sApp->mMusicInterface->Update();
	}

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pSetClipRect
//--------------------------------------------------
PyObject* PycapApp::pSetClipRect( PyObject* self, PyObject* args )
{
	// parse the arguments
	int x, y, w, h;
	if( !PyArg_ParseTuple( args, "iiii", &x, &y, &w, &h ) )
        return NULL;

	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "SetClipRect() failed: Not currently drawing!" ) );
		return NULL;
	}
	// set the clip region
	graphics->SetClipRect( x,y,w,h );

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pClearClipRect
//--------------------------------------------------
PyObject* PycapApp::pClearClipRect( PyObject* self, PyObject* args )
{
	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "ClearClipRect() failed: Not currently drawing!" ) );
		return NULL;
	}
	// clear the clip region
	graphics->ClearClipRect();

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pSetTranslation
//--------------------------------------------------
PyObject* PycapApp::pSetTranslation( PyObject* self, PyObject* args )
{
	// parse the arguments
	int x, y;
	if( !PyArg_ParseTuple( args, "ii", &x, &y ) )
        return NULL;

	// check that we're currently drawing
	Graphics* graphics = sApp->mBoard->getGraphics();
	if( !graphics )
	{
		// fail, 'cos we can only do this while drawing
		sApp->Popup( StrFormat( "SetTranslation() failed: Not currently drawing!" ) );
		return NULL;
	}
	// set the translation
	graphics->mTransX = x;
	graphics->mTransY = y;

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pSetFullscreen
//--------------------------------------------------
PyObject* PycapApp::pSetFullscreen( PyObject* self, PyObject* args )
{
	// parse the arguments
	int fullscreen;
	if( !PyArg_ParseTuple( args, "i", &fullscreen ) )
        return NULL;

	// set fullscreen to true
	sApp->SwitchScreenMode( !fullscreen, sApp->Is3DAccelerated() );

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}

//--------------------------------------------------
// pGetFullscreen
//--------------------------------------------------
PyObject* PycapApp::pGetFullscreen( PyObject* self, PyObject* args )
{
	if( sApp->mIsWindowed )
	{
		return Py_BuildValue( "i", 0 );
	}
	else
	{
		return Py_BuildValue( "i", 1 );
	}
}

//--------------------------------------------------
// pAllowAllAccess
//--------------------------------------------------
PyObject* PycapApp::pAllowAllAccess( PyObject* self, PyObject* args )
{
	// parse the arguments
	char* fileName;
	if( !PyArg_ParseTuple( args, "s", &fileName ) )
        return NULL;

	// attempt to unlock the file
	// return whether or not we succeeded ('tho I'll probably just ignore it most of the time)
	if( AllowAllAccess( fileName ) )
	{
		// success
	    return Py_BuildValue( "i", 1 );
	}
	else
	{
		// failure
	    return Py_BuildValue( "i", 0 );
	}
}

//--------------------------------------------------
// pGetAppDataFolder
//--------------------------------------------------
PyObject* PycapApp::pGetAppDataFolder( PyObject* self, PyObject* args )
{
	// get the folder string
	std::string string = GetAppDataFolder();

	// convert foler name to a python string & return it
	return Py_BuildValue( "s", string.c_str() );
}

//--------------------------------------------------
// pPause
//--------------------------------------------------
PyObject* PycapApp::pPause( PyObject* self, PyObject* args )
{
	// parse the arguments
	int pause;
	if( !PyArg_ParseTuple( args, "i", &pause ) )
        return NULL;

	// test argument
	if( pause )
	{
//		sApp->mBoard->Pause(true);
	}
	else
	{
//		sApp->mBoard->Pause(false);
	}

	// return, 'cos we're done
	Py_INCREF( Py_None );
 	return Py_None;
}
