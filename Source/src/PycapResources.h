//--------------------------------------------------
// PycapResources
//
// Pycap application resources
// Handles access to images, sounds etc by python
//
//--------------------------------------------------

// includes
#include "SexyAppBase.h"
#include "BassMusicInterface.h"
#include "ResourceManager.h"
#include <vector>
#include <list>


#ifndef __PYCAPRESOURCES_H__
#define __PYCAPRESOURCES_H__

#include <Python.h>

struct IDirectMusicLoader;
struct IDirectMusicSegment;

// use the Sexy namespace
namespace Sexy
{

// class declarations
class Image;
class Font;
class ImageFont;
class SysFont;

class ResourceManager;

// Pycap Resources class
class PycapResources
{
	//------------
	// functions
	//------------

	public:

	// constructor / destructor
	PycapResources();			// create object and load resources. Also sets up resource management module.
	virtual ~PycapResources();	// destroy object, clean up resources. This probably shouldn't leave the res management module active, but it does. Meh.

	// resource accessors
	Image*					getImage( int index );
	Font*					getFont( int index );
	bool					soundExists( int index );
	bool					musicExists( int index );
	IDirectMusicSegment*	getTune( int index );

	private:

	// resource loading
	Image*					loadImage( const std::string& fileName );			// attempt load an image and convert pallete
	Font*					loadFont( const std::string& fileName );			// attempt load an image font
	Font*					sysFont(											// attempt to create a system font
								const std::string& faceName,
								int pointSize,
								int script,
								bool bold,
								bool italics,
								bool underline
								);
	
    bool					loadSound( int id, const std::string& fileName );	// attempt load a sound into a given slot
    bool					loadSound( int id, int songId );	                // attempt load a sound resource into a given slot

	bool					loadMusic( int id, const std::string& fileName );	// attempt load a sound into a given slot
	IDirectMusicSegment*	loadTune( const std::string& fileName );			// attempt load a midi file

	// Python resource handling functions
	// Resources are simply referenced by index. Failed calls pop message boxes and throw exceptions.
	static PyObject* pLoadImage( PyObject* self, PyObject* args );		        // attempt to load an image and convert palette. Returns image index on success.
	static PyObject* pImageWidth( PyObject* self, PyObject* args );		        // get width of an image resource.
	static PyObject* pImageHeight( PyObject* self, PyObject* args );	        // get height of an image resource.
	static PyObject* pUnloadImage( PyObject* self, PyObject* args );	        // attempt to unload a given image.
	static PyObject* pLoadFont( PyObject* self, PyObject* args );		        // attempt to load a font. Returns font index on success.
	static PyObject* pSysFont( PyObject* self, PyObject* args );		        // attempt to create a system font. Returns font index on success.
	static PyObject* pStringWidth( PyObject* self, PyObject* args );	        // get width of a string if drawn with a specific font.
	static PyObject* pFontAscent( PyObject* self, PyObject* args );		        // get ascent of a font.
	static PyObject* pUnloadFont( PyObject* self, PyObject* args );		        // attempt to unload a given font.
	static PyObject* pSetFontScale( PyObject* self, PyObject* args );	        // set the draw scale of an image font object
	static PyObject* pLoadSound( PyObject* self, PyObject* args );		        // attempt to load a sound.
	static PyObject* pUnloadSound( PyObject* self, PyObject* args );	        // attempt to unload a given sound.
	static PyObject* pLoadTune( PyObject* self, PyObject* args );		        // attempt to load a midi file
	static PyObject* pUnloadTune( PyObject* self, PyObject* args );		        // attempt to unload a given midi file.
	static PyObject* pLoadMusic( PyObject* self, PyObject* args );		        // attempt to load a music file
	static PyObject* pUnloadMusic( PyObject* self, PyObject* args );	        // attempt to unload a given music file.
	static PyObject* pLoadResourceManifest( PyObject* self, PyObject* args );	// attempt to load resource manifest file.
	static PyObject* pLoadResources( PyObject* self, PyObject* args );	        // attempt to load resources from mResourceManager.
    static PyObject* pGetNumResources( PyObject* self, PyObject* args );	    // attempt to get resources count from one resources group.
    static PyObject* pStartLoadResources( PyObject* self, PyObject* args );	    // attempt to start load resource from one resources group.
    static PyObject* pLoadNextResource( PyObject* self, PyObject* args );	    // attempt to load next resource from one resources group.
    static PyObject* pLoadResourceHadError( PyObject* self, PyObject* args );	// attempt to check had error on load resource from one resources group.
    static PyObject* pGetImage( PyObject* self, PyObject* args );	            // attempt to get a image resource with id.
    static PyObject* pGetSound( PyObject* self, PyObject* args );	            // attempt to get a image resource with id.
    static PyObject* pGetFont( PyObject* self, PyObject* args );	            // attempt to get a image resource with id.


	//----------
	// members
	//----------

	public:

	// instance
	static PycapResources* sRes;	// reference to self. Assumes use as a singleton (very ugly)

	private:

	// resources
	std::vector<Image*>					images;		// collection of image objects
	std::list<int>						freeImages;	// list of empty image slots
	std::vector<Font*>					fonts;		// collection of font objects
	std::list<int>						freeFonts;	// list of empty font slots
	std::vector<bool>					sounds;		// flags indicating whether sounds are valid
	std::list<int>						freeSounds;	// list of empty sound slots
	std::vector<IDirectMusicSegment*>	tunes;		// collection of music segment objects
	std::list<int>						freeTunes;	// list of empty music segment slots
	std::vector<bool>					musics;		// flags indicating whether musics are valid
	std::list<int>						freeMusics;	// list of empty musics slots


	// functional
	IDirectMusicLoader*		musicLoader;	// direct music loader object
};

}

#endif // __PYCPRESOURCES_H__