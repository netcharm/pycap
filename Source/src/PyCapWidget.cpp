//--------------------------------------------------
// PycapWidget
//
// Base game widget for Pycap
// Manages widget hooks to python game code
//
//--------------------------------------------------

// includes
#include "PycapWidget.h"
#include "PycapApp.h"

#include "Graphics.h"
#include <Python.h>


#include <Math.h>


// namespace
using namespace Sexy;

// struct define
typedef struct {
    PyObject_HEAD
    PyObject *app; /* widget's parent app object */
    PyObject *id; /* widget's id */
    PyObject *listener;  /* widget's listener */
} PycapWidget;

static void PycapWidget_dealloc(PycapWidget* self)
{
    Py_XDECREF(self->first);
    Py_XDECREF(self->last);
    self->ob_type->tp_free((PyObject*)self);
}

static PyObject* PycapWidget_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    PycapWidget* self;

    self = (PycapWidget*)type->tp_alloc(type, 0);
    if (self != NULL) {
        self->first = PyString_FromString("");
        if (self->first == NULL)
          {
            Py_DECREF(self);
            return NULL;
          }

        self->last = PyString_FromString("");
        if (self->last == NULL)
          {
            Py_DECREF(self);
            return NULL;
          }

        self->number = 0;
    }

    return (PyObject *)self;
}

static int PycapWidget_init(PycapWidget *self, PyObject *args, PyObject *kwds)
{
    PyObject* app=NULL, listener=NULL, tmp;

    static char *kwlist[] = {"app", "id", "listener", NULL};

    if (! PyArg_ParseTupleAndKeywords(args, kwds, "|OiO", kwlist,
                                      &app, &id,
                                      &listener))
        return -1;

    if (app) {
        //tmp = self->first;
        //Py_INCREF(first);
        //self->first = first;
        //Py_XDECREF(tmp);
    }

    if (id) {
        //tmp = self->last;
        //Py_INCREF(last);
        //self->last = last;
        //Py_XDECREF(tmp);
    }

    if (listener) {
        //tmp = self->last;
        //Py_INCREF(last);
        //self->last = last;
        //Py_XDECREF(tmp);
    }

    return 0;
}

static PyMethodDef PycapWidget_methods[] = {
    {"name", (PyCFunction)PyTypeObject_name, METH_NOARGS, "Return the name, combining the first and last name"},
    {NULL}  /* Sentinel */
};

static PyTypeObject PycapWidgetType = {
    PyObject_HEAD_INIT(NULL)
    0,                                          /*ob_size*/
    "PyCap.Widget",                             /*tp_name*/
    sizeof(PycapWidget),                        /*tp_basicsize*/
    0,                                          /*tp_itemsize*/
    (destructor)PycapWidget_dealloc,            /*tp_dealloc*/
    0,                                          /*tp_print*/
    0,                                          /*tp_getattr*/
    0,                                          /*tp_setattr*/
    0,                                          /*tp_compare*/
    0,                                          /*tp_repr*/
    0,                                          /*tp_as_number*/
    0,                                          /*tp_as_sequence*/
    0,                                          /*tp_as_mapping*/
    0,                                          /*tp_hash */
    0,                                          /*tp_call*/
    0,                                          /*tp_str*/
    0,                                          /*tp_getattro*/
    0,                                          /*tp_setattro*/
    0,                                          /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,   /*tp_flags*/
    "PyCap Widget Object",                      /* tp_doc */
    0,		                                      /* tp_traverse */
    0,		                                      /* tp_clear */
    0,		                                      /* tp_richcompare */
    0,		                                      /* tp_weaklistoffset */
    0,		                                      /* tp_iter */
    0,		                                      /* tp_iternext */
    PycapWidget_methods,                        /* tp_methods */
    PycapWidget_members,                        /* tp_members */
    0,                                          /* tp_getset */
    0,                                          /* tp_base */
    0,                                          /* tp_dict */
    0,                                          /* tp_descr_get */
    0,                                          /* tp_descr_set */
    0,                                          /* tp_dictoffset */
    (initproc)PycapWidget_init,                 /* tp_init */
    0,                                          /* tp_alloc */
    PycapWidget_new,                            /* tp_new */
};

static PyMethodDef module_methods[] = {
    {NULL}  /* Sentinel */
};

// functions

//--------------------------------------------------
// PycapWidget
//--------------------------------------------------
PycapWidget::PycapWidget()
{
	// call python game init function
	PyObject* pInitFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "init" );

  if ( pInitFunc )
	{
		if ( PyCallable_Check( pInitFunc ) )
		{
			PyObject_CallObject( pInitFunc, NULL );
		}
		else
		{
			PycapApp::sApp->Popup( StrFormat( "\"init\" found, but not callable" ) );
		}
	}

	// grab frequently used python functions
	pUpdateFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "update" );
	pDrawFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "draw" );
	pKeyDownFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "keyDown" );
	pKeyUpFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "keyUp" );
	pExitWidget = PyDict_GetItemString( PycapApp::sApp->pDict, "exitWidget" );
	pMouseEnterFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "mouseEnter" );
	pMouseLeaveFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "mouseLeave" );
	pMouseMoveFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "mouseMove" );
	pMouseDownFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "mouseDown" );
	pMouseUpFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "mouseUp" );
	pMouseWheelFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "mouseWheel" );
	// legacy spelling
	if( !pKeyDownFunc )
		pKeyDownFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "keydown" );
	if( !pKeyUpFunc )
		pKeyUpFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "keyup" );


	// init remaining members
	graphics	= NULL;

	// general error location warning
	if (PyErr_Occurred())
	{
		PyErr_Print();
		PycapApp::sApp->Popup( StrFormat( "Some kind of python error occurred in PycapWidget()." ) );
	}

	// request initial draw
	MarkDirty();
}

//--------------------------------------------------
// ~PycapWidget
//--------------------------------------------------
PycapWidget::~PycapWidget()
{
	// call python shutdown function
	PyObject* pFiniFunc = PyDict_GetItemString( PycapApp::sApp->pDict, "fini" );

  if ( pFiniFunc && PyCallable_Check( pFiniFunc ) )
	{
        PyObject_CallObject( pFiniFunc, NULL );
  }
	// general error location warning
	if (PyErr_Occurred())
	{
		PyErr_Print();
		PycapApp::sApp->Popup( StrFormat( "Some kind of python error occurred in ~PycapWidget()." ) );
	}
}

//--------------------------------------------------
// Update
//--------------------------------------------------
void PycapWidget::UpdateF( float delta )
{
	// call parent
	Widget::UpdateF( delta );

	//PycapApp::sApp->mMusicInterface->Update();

	// Python exit-check
	// Checked on entering incase a non-update function has set it
	if ( pExitWidget )
	{
		PyObject* pExit = PyObject_CallObject( pExitWidget, NULL );
		if ( PyInt_Check( pExit ) && PyInt_AsLong( pExit ) != 0 )
		{
			// drop the return value
			Py_DECREF( pExit );

			// exit the widget
			//PycapApp::sApp->Shutdown();

			// no need to update
			return;
		}
		// drop the return value
		Py_DECREF( pExit );
	}

	// Python update hook
	// The python code should call dirty if the screen needs to be redrawn
	if ( pUpdateFunc )
	{
		PyObject* pArgs = PyTuple_New(1);
		PyObject* pDelta = PyFloat_FromDouble( delta );
		PyTuple_SetItem( pArgs, 0, pDelta );
		PyObject_CallObject( pUpdateFunc, pArgs );
		Py_DECREF( pDelta );
		Py_DECREF( pArgs );
	}

	// widget exit-check
	// Checked on exiting updatef incase it has set it
	if ( pExitWidget )
	{
		PyObject* pExit = PyObject_CallObject( pExitWidget, NULL );
		if ( PyInt_Check( pExit ) && PyInt_AsLong( pExit ) != 0 )
		{
			// drop the return value
			Py_DECREF( pExit );

			// exit the widget
			//PycapApp::sApp->Shutdown();

			// no need to update
			return;
		}
		// drop the return value
		Py_DECREF( pExit );
	}

	// general error location warning
	if (PyErr_Occurred())
	{
		PyErr_Print();
		PycapApp::sApp->Popup( StrFormat( "Some kind of python error occurred in Update." ) );
		PycapApp::sApp->Shutdown();
	}
}

//--------------------------------------------------
// Draw
//--------------------------------------------------
void PycapWidget::Draw( Graphics *g )
{
	// exit early if no python draw function
	if ( !pDrawFunc )
		return;

	// enter draw code
	graphics = g;

	// call draw function
    PyObject_CallObject( pDrawFunc, NULL );

	// exit draw code
	graphics = NULL;

	// general error location warning
	if (PyErr_Occurred())
	{
		PyErr_Print();
		PycapApp::sApp->Popup( StrFormat( "Some kind of python error occurred in Draw." ) );
		PycapApp::sApp->Shutdown();
	}
}

//--------------------------------------------------
// KeyDown
//--------------------------------------------------
void PycapWidget::KeyDown( KeyCode key )
{
	// exit early if no python keydown function
	if ( !pKeyDownFunc )
		return;

	// Python keydown hook
	PyObject* pArgs = PyTuple_New(1);
	PyObject* pKey = PyInt_FromLong( key );
	PyTuple_SetItem( pArgs, 0, pKey );
    PyObject_CallObject( pKeyDownFunc, pArgs );
	Py_DECREF( pArgs );

	// general error location warning
	if (PyErr_Occurred())
	{
		PyErr_Print();
		PycapApp::sApp->Popup( StrFormat( "Some kind of python error occurred in KeyDown." ) );
		PycapApp::sApp->Shutdown();
	}
}

//--------------------------------------------------
// KeyUp
//--------------------------------------------------
void PycapWidget::KeyUp( KeyCode key )
{
	// exit early if no python keyup function
	if ( !pKeyUpFunc )
		return;

	// Python keyup hook
	PyObject* pArgs = PyTuple_New(1);
	PyObject* pKey = PyInt_FromLong( key );
	PyTuple_SetItem( pArgs, 0, pKey );
    PyObject_CallObject( pKeyUpFunc, pArgs );
	Py_DECREF( pArgs );

	// general error location warning
	if (PyErr_Occurred())
	{
		PyErr_Print();
		PycapApp::sApp->Popup( StrFormat( "Some kind of python error occurred in KeyUp." ) );
		PycapApp::sApp->Shutdown();
	}
}

//--------------------------------------------------
// MouseEnter
//--------------------------------------------------
void PycapWidget::MouseEnter()
{
	// call python function if it exists
	if( pMouseEnterFunc )
		PyObject_CallObject( pMouseEnterFunc, NULL );

	// general error location warning
	if (PyErr_Occurred())
	{
		PyErr_Print();
		PycapApp::sApp->Popup( StrFormat( "Some kind of python error occurred in MouseEnter." ) );
		PycapApp::sApp->Shutdown();
	}
}

//--------------------------------------------------
// MouseLeave
//--------------------------------------------------
void PycapWidget::MouseLeave()
{
	// call python function if it exists
	if( pMouseLeaveFunc )
		PyObject_CallObject( pMouseLeaveFunc, NULL );

	// general error location warning
	if (PyErr_Occurred())
	{
		PyErr_Print();
		PycapApp::sApp->Popup( StrFormat( "Some kind of python error occurred in MouseLeave." ) );
		PycapApp::sApp->Shutdown();
	}
}

//--------------------------------------------------
// MouseMove
//--------------------------------------------------
void PycapWidget::MouseMove( int x, int y )
{
	// Python mouse move hook
	if( pMouseMoveFunc )
	{
		PyObject* pArgs = PyTuple_New(2);
		PyObject* pX = PyInt_FromLong( x );
		PyObject* pY = PyInt_FromLong( y );
		PyTuple_SetItem( pArgs, 0, pX );
		PyTuple_SetItem( pArgs, 1, pY );
		PyObject_CallObject( pMouseMoveFunc, pArgs );
		Py_DECREF( pArgs );

		// general error location warning
		if (PyErr_Occurred())
		{
			PyErr_Print();
			PycapApp::sApp->Popup( StrFormat( "Some kind of python error occurred in MouseMove." ) );
			PycapApp::sApp->Shutdown();
		}
	}
}

//--------------------------------------------------
// MouseDrag
//--------------------------------------------------
void PycapWidget::MouseDrag( int x, int y )
{
	// This gets called instead of mousemove when dragging.
	// For our purposes, they're the same... so do the same thing!
	MouseMove( x, y );
}

//--------------------------------------------------
// MouseDown
//--------------------------------------------------
void PycapWidget::MouseDown(int x, int y, int theBtnNum, int theClickCount)
{
	// Python mouse down hook
	if( pMouseDownFunc )
	{
		PyObject* pArgs = PyTuple_New(3);
		PyObject* pX = PyInt_FromLong( x );
		PyObject* pY = PyInt_FromLong( y );
		PyObject* pButton = PyInt_FromLong( theBtnNum );
		PyTuple_SetItem( pArgs, 0, pX );
		PyTuple_SetItem( pArgs, 1, pY );
		PyTuple_SetItem( pArgs, 2, pButton );
		PyObject_CallObject( pMouseDownFunc, pArgs );
		Py_DECREF( pArgs );

		// general error location warning
		if (PyErr_Occurred())
		{
			PyErr_Print();
			PycapApp::sApp->Popup( StrFormat( "Some kind of python error occurred in MouseDown." ) );
			PycapApp::sApp->Shutdown();
		}
	}
}

//--------------------------------------------------
// MouseUp
//--------------------------------------------------
void PycapWidget::MouseUp(int x, int y, int theBtnNum, int theClickCount)
{
	// Python mouse up hook
	if( pMouseUpFunc )
	{
		PyObject* pArgs = PyTuple_New(3);
		PyObject* pX = PyInt_FromLong( x );
		PyObject* pY = PyInt_FromLong( y );
		PyObject* pButton = PyInt_FromLong( theBtnNum );
		PyTuple_SetItem( pArgs, 0, pX );
		PyTuple_SetItem( pArgs, 1, pY );
		PyTuple_SetItem( pArgs, 2, pButton );
		PyObject_CallObject( pMouseUpFunc, pArgs );
		Py_DECREF( pArgs );

		// general error location warning
		if (PyErr_Occurred())
		{
			PyErr_Print();
			PycapApp::sApp->Popup( StrFormat( "Some kind of python error occurred in MouseUp." ) );
			PycapApp::sApp->Shutdown();
		}
	}
}

//--------------------------------------------------
// MouseWheel
//--------------------------------------------------
void PycapWidget::MouseWheel( int delta)
{
	// Python mouse move hook
	if( pMouseWheelFunc )
	{
		PyObject* pArgs = PyTuple_New(1);
		PyObject* pX = PyInt_FromLong( delta );
		PyTuple_SetItem( pArgs, 0, pX );
		PyObject_CallObject( pMouseWheelFunc, pArgs );
		Py_DECREF( pArgs );

		// general error location warning
		if (PyErr_Occurred())
		{
			PyErr_Print();
			PycapApp::sApp->Popup( StrFormat( "Some kind of python error occurred in MouseWheel." ) );
			PycapApp::sApp->Shutdown();
		}
	}
}