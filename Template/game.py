########################################################################
# Template PyCap Game v1.0                                             #
# Written by NetCharm                                                  #
# Written @ 2011-05-01                                                 #
# -*- coding: utf-8 -*-                                                #
########################################################################

import os
import sys
import time
import random
import math

appIni = {  "mCompanyName"         : "NetCharm",
            "mFullCompanyName"     : "NetCharm Studio!",
            "mProdName"            : "PyCap Template Game",
            "mProductVersion"      : "1.0",
            "mTitle"               : "PyCap Template Game v1.0",
            "mRegKey"              : "PyCapTemplate",
            "mWidth"               : 800,
            "mHeight"              : 600,
            "mAutoEnable3D"        : 1,
            "mVSyncUpdates"        : 1,
            "mWantFMod"            : 1,
            "mNoSoundNeeded"       : 0,
            "mRecordingDemoBuffer" : 1,
            "mReadFromRegistry"    : 1,
            "mDebugKeysEnabled"    : 1 }

import PyCap as PC
PCR = None

def loadBase():
  import PyCapRes
  global PCR
  PCR = PyCapRes

def init():
	pass

def update( delta ):
	PC.markDirty()

def draw():
	pass
