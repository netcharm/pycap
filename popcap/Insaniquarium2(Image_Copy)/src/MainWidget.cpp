#include <time.h>
#include "FishButtonWidget.h"
#include "Fish.h"

#include "GameApp.h"
#include "MainWidget.h"
#include "OptionsDialog.h"

using namespace Sexy;

MainWidget::MainWidget(GameApp* theApp):mCel(0),mSleep(0),Yux(50),Yuy(100),mWCel(1),i(1),mBCel(0),mBx(-1),mAniSleep(0)
,mNCel(1),mNikoSleep(0),bi(1),ii(1),Yuh(0),mPause(false),mDrawFun(NULL)
{
	anApp=theApp;
	Resize(0,0,anApp->mWidth,anApp->mHeight);
	mLightImage=NULL;
	mWidth=anApp->mWidth;
	mHeight=anApp->mHeight;

}

MainWidget::~MainWidget()
{
	delete mPrestoButton;
	delete mGuBiButton;
	delete mAnthButton;
	delete mBlipButton;

	delete mMenuButton;

	for(size_t i=0;i<mFish.size();i++)
		delete mFish[i];

}

void MainWidget::KeyDown(KeyCode theKey)
{
	if(theKey==KEYCODE_ESCAPE)
	{
		anApp->Shutdown();
	}

}

void MainWidget::ButtonDepress(int theId)
{
	if(theId==mMenuButton->mId)
	{
		anApp->PlaySample(mButtonClickSound);
        OptionsDialog* dlg = new OptionsDialog(anApp,_T("Options(V:0.1)"),SPACE);
		dlg->Resize(mWidth / 2 -150, 50, 300, 400);
		anApp->AddDialog(OptionsDialog::DIALOG_ID, dlg);

	}
	else
	{
		anApp->PlaySample(mNewFishSound);
		//我可怜的老爷机
		if(mFish.size()<32)
		{
			if(theId==mGuBiButton->mId)
			{
				Fish* mFish_GuBi=new Fish(anApp,mFishImage_GuBi,mTurnFishImage_GuBi,0);
				mFish.push_back(mFish_GuBi);
			}
			else if(theId==mAnthButton->mId)
			{
				Fish* mFish_Anth=new Fish(anApp,mFishImage_GuBi,mTurnFishImage_GuBi,4);
				mFish.push_back(mFish_Anth);
			}
			else if(theId==mPrestoButton->mId)
			{
				Fish* mFish_Presto=new Fish(anApp,mFishImage_Presto,anApp->mTurnFishImage_Presto,0);
				mFish.push_back(mFish_Presto);

			}
			else if(theId==mBlipButton->mId)
			{
				Fish* mFish_Blip=new Fish(anApp,mFishImage_Blip,anApp->mTurnFishImage_Blip,0);
				mFish.push_back(mFish_Blip);

			}

			anApp->PlaySample(mWaterSound);
		}
	}


}

void MainWidget::Update()
{
	Widget::Update();

	MarkDirty();
}

void MainWidget::DrawBackground(Graphics *g)
{
	//画背景
	g->DrawImage(mBGImage,0,0);
	g->DrawImage(mCommandHeadImage,0,0);


	g->SetColorizeImages(true);
	g->SetColor(Color(255,255,255,150));
	//画鱼群
	g->DrawImage(mFishsImage,mYusX,160);

	//水中倒影
	g->SetColor(Color(255, 255, 255, 90));

	g->DrawImage(mRefImage,0,mHeight-mRefImage->mHeight,mWidth,mRefImage->mHeight);
	g->SetColorizeImages(false);

	//河蚌
	g->DrawImageCel(mNikoImage,95,255,mNCel);


	//水波纹
	g->SetDrawMode(Graphics::DRAWMODE_ADDITIVE);
	g->DrawImageCel(mWaterMaskImage,Rect(0,80,mWidth,mWaterMaskImage->GetCelHeight()),mWCel);

	//最好写成Class也就没那么丑陋,管他呢.只是演示使用方法而已...
	if(mBx!=-1)
	{
		//当然最好做成贝赛尔,但原版也是抖动处理.这里也就偷懒了.
		g->DrawImageCel(mBubble,mBx+rand()%4+bi,mBy,mBCel);
		g->DrawImageCel(mBubble,mBx-rand()%4-bi,mBy+12,mBCel-1);
		g->DrawImageCel(mBubble,mBx+rand()%4+bi,mBy+23,mBCel+1);
		g->DrawImageCel(mBubble,mBx-rand()%4-bi,mBy+35,mBCel-1);

	}

	g->SetDrawMode(Graphics::DRAWMODE_NORMAL);

	//光线
	g->SetColor(Color(255, 255, 255, 60));
	g->SetColorizeImages(true);
	g->DrawImage(mLightImage,15,13);
	g->SetColorizeImages(false);

}

void MainWidget::Draw(Graphics *g)
{
	g->SetColor(Color::Black);
	g->FillRect(0,0,mWidth,mHeight);

	DrawBackground(g);

	!mPause?DrawFish(g):DrawPause(g);
}


void MainWidget::DrawPause(Graphics* g)
{
	g->SetFont(BIGDEFAULTFONT);
	g->SetColor(Color(0,255,0,255));
	g->DrawString(Pause_Text,(int)(mWidth / 2 - lstrlen(Pause_Text) / 2 * (BIGDEFAULTFONT->CharWidth('A'))),(mHeight + mCommandHeadImage->mHeight) / 2 - BIGDEFAULTFONT->GetHeight() / 2);
}

void MainWidget::DrawFish(Graphics* g)
{

	for(size_t i=0;i<mFish.size();i++)
		mFish[i]->Draw(g);
}

void MainWidget::CreateFishButtonAndAddWidget(FishButtonWidget **FishButton,const int theId,const int ButtonLocalX,Image* theImage)
{
	*FishButton=new FishButtonWidget(theId,this);

	(*FishButton)->mBtnImage=theImage;
	(*FishButton)->mMaskBtnImage=mBtnMaskImage;
	(*FishButton)->Resize(ButtonLocalX,3,55,60);
	(*FishButton)->mButtonImage=mBtnImage;
	(*FishButton)->mOverImage=mBtnActiveImage;
	(*FishButton)->mDownImage=mBtnPressImage;
	(*FishButton)->mDoFinger=true;
	mFishButton.push_back(*FishButton);
	anApp->mWidgetManager->AddWidget(*FishButton);

}

void MainWidget::AddedToManager(WidgetManager* theWidgetManager)
{
	Widget::AddedToManager(theWidgetManager);

	mLightImage=mLightImage1;
	mFishsImage=mFishsImage1;
	mYusX=mWidth-mFishsImage->mWidth;
	mRefImage=mRefImage1;

	mMenuButton=new ButtonWidget(5,this);
	mMenuButton->Resize(Rect(525,3,mBtnMenuPushImage->mWidth,mBtnMenuPushImage->mHeight));
	mMenuButton->mDownImage=mBtnMenuPushImage;
	mMenuButton->mOverImage=mBtnMenuActiveImage;
	mMenuButton->mDoFinger=true;

	theWidgetManager->AddWidget(mMenuButton);

	CreateFishButtonAndAddWidget(&mGuBiButton,1,19,anApp->mBtnFishImage_GuBi);
	CreateFishButtonAndAddWidget(&mPrestoButton,2,364,mBtnFishImage_Presto);
	CreateFishButtonAndAddWidget(&mAnthButton,3,145,mBtnFishImage_Anth);
	CreateFishButtonAndAddWidget(&mBlipButton,4,437,mBtnFishImage_Blip);

	anApp->PlayMusic(GameApp::mBG2);

	AddTank();

}

void MainWidget::AddTank()
{

	Fish* mFish_Rhubarb=new Fish(anApp,mTankImage_Rhubarb,anApp->mTurnTankImage_Rhubarb,0,true);
	mFish_Rhubarb->mFishSleep=30;
	mFish_Rhubarb->mSpeed=3;
	mFish.push_back(mFish_Rhubarb);

	Fish* mFish_Rufus=new Fish(anApp,mTankImage_Rufus,anApp->mTurnTankImage_Rufus,0,true,348);
	mFish_Rufus->mFishSleep=50;
	mFish.push_back(mFish_Rufus);

}

void MainWidget::RemovedFromManager(WidgetManager* theWidgetManager)
{
	Widget::RemovedFromManager(theWidgetManager);


	theWidgetManager->RemoveWidget(mMenuButton);

	theWidgetManager->RemoveWidget(mGuBiButton);		
	theWidgetManager->RemoveWidget(mPrestoButton);
	theWidgetManager->RemoveWidget(mAnthButton);
	theWidgetManager->RemoveWidget(mBlipButton);

}

void MainWidget::UpdateF(float theFrac)
{

	//改用延时函数能有效降低CPU的使用率,这里偷懒就用累加器了.
	if (!mPause)
	{
		if(mSleep++==5)
		{
			bi=-bi;
			if(mCel++==mFishImage_GuBi->mNumCols-1)
				mCel=0;

			if( (mWCel==mWaterMaskImage->mNumRows-1) || mWCel==0)
			{
				i=-i;
			}
			mWCel+=i;

			mSleep=0;

			if(!(mBy<100))
				mBy-=12;
			else
				mBx=-1;

			if(rand()%200==100)
			{
				if(mBx==-1)
				{
					srand((int)time(NULL));
					anApp->PlaySample(mBubblesSound);
					mBCel=rand()%4;
					mBx=rand()%mWidth;
					mBy=mHeight;
				}

			}

			if(mFishsImage==mFishsImage1)
				mFishsImage=mFishsImage2;
			else
				mFishsImage=mFishsImage1;

		}

		if(mNikoSleep++==2)
		{
			if( (mNCel==mNikoImage->mNumCols-1) || mNCel==0)
			{
				ii=-ii;
			}
			mNCel+=ii;

			mNikoSleep=0;
		}

		if(mAniSleep++==1200)
		{
			if(mLightImage==mLightImage1)
				mLightImage=mLightImage2;
			else
				mLightImage=mLightImage1;

			if(mRefImage==mRefImage1)
				mRefImage=mRefImage2;
			else
				mRefImage=mRefImage1;

			mAniSleep=0;

		}

		if (mYusX--<-(mFishsImage->mWidth))
			mYusX=mWidth;


		for(size_t i=0;i<mFish.size();i++)
			mFish[i]->Update();
	}

}

void MainWidget::MouseDown(int x, int y, int theClickCount)
{
	Widget::MouseDown(x, y, theClickCount);

	//是否想让鱼儿过来呢?
	if (theClickCount < 0)
		anApp->PlaySample(mGlassTagSound);
}

void MainWidget::Pause(bool p)
{
	mPause=p;
	for(size_t i=0;i<mFishButton.size();i++)
		mFishButton[i]->Pause(p);
	if(p)
		MarkDirty();

}