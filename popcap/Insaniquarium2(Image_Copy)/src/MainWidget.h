#ifndef __MAINWIDGET__
#define __MAINWIDGET__

#include <Widget.h>
#include <ButtonListener.h>
#include <vector>


namespace Sexy
{
	class GameApp;
	class Graphics;
	class Image;
	class ButtonWidget;
	class FishButtonWidget;
	class WidgetManager;
	class Fish;

	typedef void (*DrawFun)(Graphics*);

	class MainWidget:public Widget,public ButtonListener
	{
	private:
		bool mPause;
		GameApp* anApp;
        int mCel,mWCel,mSleep,mAniSleep,mNikoSleep,mNCel,i,ii,bi,mBx,mBy,mBCel,mYusX;
		int Yux,Yuy;
		int Yuh;
		KeyCode mPrevKeyCode;
		Image* mLightImage;
		Image* mFishsImage;
		Image* mRefImage;

		FishButtonWidget* mGuBiButton;
		FishButtonWidget* mPrestoButton;
		FishButtonWidget* mAnthButton;
		FishButtonWidget* mBlipButton;

		ButtonWidget* mMenuButton;
		DrawFun mDrawFun;
		std::vector<Fish*> mFish;
		std::vector<FishButtonWidget*> mFishButton;
	public:
		MainWidget(GameApp* theApp);
		virtual ~MainWidget();

		virtual void Update();
		virtual void Draw(Graphics* g);
		virtual void UpdateF(float theFrac);
		virtual void KeyDown(KeyCode theKey);
		virtual void AddedToManager(WidgetManager* theWidgetManager);
		virtual void RemovedFromManager(WidgetManager* theWidgetMangaer);
		virtual void ButtonDepress(int theId);
		virtual void MouseDown(int x, int y, int theClickCount);
		void AddTank();
		void CreateFishButtonAndAddWidget(FishButtonWidget **,const int,const int,Image*);
		void DrawBackground(Graphics*);
		void DrawFish(Graphics*);
		void DrawPause(Graphics*);
		void Pause(bool p);

	};

}

#endif
