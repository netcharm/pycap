#include <Graphics.h>
#include "GameApp.h"
#include "FishButtonWidget.h"


using namespace Sexy;

FishButtonWidget::FishButtonWidget(int theId,ButtonListener* theButtonListener):ButtonWidget(theId,theButtonListener),mCel(0),mSleep(0)
,mPause(false)
{


}



FishButtonWidget::~FishButtonWidget()
{
}

void FishButtonWidget::Update()
{
	ButtonWidget::Update();



	MarkDirty();

}

void FishButtonWidget::UpdateF(float theFrac)
{
	if(!mPause)
		if(mSleep++==5)
		{
			if(mCel++==mBtnImage->mNumCols-1)
				mCel=0;

			mSleep=0;

		}
}

void FishButtonWidget::Draw(Graphics* g)
{
	ButtonWidget::Draw(g);

	g->SetDrawMode(Graphics::DRAWMODE_ADDITIVE);
	g->SetColor(Color::White);
	g->SetColorizeImages(true);
	g->DrawImage(mMaskBtnImage,0,0);
	g->SetColorizeImages(false);
	g->SetDrawMode(Graphics::DRAWMODE_NORMAL);

	g->DrawImageCel(mBtnImage,Rect(10,4,40,40),mCel);


}

void FishButtonWidget::Pause(bool p)
{
	mPause=p;

}