///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                      RainStudio
//
//本代码包括所用资源仅供演示,任何作其他用途的责任自负与本人无关.
//更多资源代码请访问Blog:http://www.RainCoding.com/blog
//
//本代码演示了PopCap的代表作之一的(Insaniquarium2 Deluxe),这里仅仅模范了图像.
//如果要加上完整的游戏逻辑工作量应该也不会太大.本代码演示图像画面比原版有所增强(加个几个背景动画而已)
//希望本代码对你学习SexyFramework有所帮助
//本代码注释不多(没有什么技术难度)所以自己看看引擎的使用方法就足够了,至于为了展示画面所加的与引擎无关的逻辑代码
//一律可以无视^_^(所有代码均没有考虑到各种情况下的容错处理且也未采取对代码逻辑的任何优化措施)
//最后更新 08-01-6
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "GameApp.h"

using namespace Sexy;

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE,LPSTR,int)
{
	HICON hIcon=LoadIcon(hInstance,"MainIcon");
	
	//几乎所有的Sexy都是这样
	GameApp* app=new GameApp;

	//Init
	app->Init();

	//设置Icon
	SendMessage(app->mHWnd,WM_SETICON,ICON_BIG,(LPARAM)hIcon);
	
	//开始循环,返回也就意味着游戏退出.
	app->Start();
	delete app;

	return 0;

}