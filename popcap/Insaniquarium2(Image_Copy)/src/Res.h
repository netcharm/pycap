#ifndef __Res_H__
#define __Res_H__

namespace Sexy
{
	class ResourceManager;
	class Image;
	class Font;

	Image* LoadImageById(ResourceManager *theManager, int theId);
	void ReplaceImageById(ResourceManager *theManager, int theId, Image *theImage);
	bool ExtractResourcesByName(ResourceManager *theManager, const char *theName);

	// Game Resources
	bool ExtractGameResources(ResourceManager *theMgr);
	extern Font* BIGDEFAULTFONT;
	extern Font* DEFAULTFONT;
	extern Font* DIALOGFONT;
	extern Image* mBGImage;
	extern Image* mBtnActiveImage;
	extern Image* mBtnFishImage_Anth;
	extern Image* mBtnFishImage_Blip;
	extern Image* mBtnFishImage_Presto;
	extern Image* mBtnImage;
	extern Image* mBtnMaskImage;
	extern Image* mBtnMenuActiveImage;
	extern Image* mBtnMenuPushImage;
	extern Image* mBtnPressImage;
	extern Image* mBubble;
	extern Image* mCheckButton;
	extern Image* mCommandHeadImage;
	extern Image* mDialogPanel;
	extern Image* mFishImage_Blip;
	extern Image* mFishImage_GuBi;
	extern Image* mFishImage_Presto;
	extern Image* mFishsImage1;
	extern Image* mFishsImage2;
	extern Image* mLightImage1;
	extern Image* mLightImage2;
	extern Image* mNikoImage;
	extern Image* mOptionsButton;
	extern Image* mRefImage1;
	extern Image* mRefImage2;
	extern Image* mSliderTrack;
	extern Image* mSliderWidget;
	extern Image* mTankImage_Rhubarb;
	extern Image* mTankImage_Rufus;
	extern Image* mTurnFishImage_GuBi;
	extern Image* mUnCheckButton;
	extern Image* mWaterMaskImage;
	extern int mBubblesSound;
	extern int mButtonClickSound;
	extern int mGlassTagSound;
	extern int mNewFishSound;
	extern int mWaterSound;

	// Init Resources
	bool ExtractInitResources(ResourceManager *theMgr);
	extern Image* mLoadbar;
	extern Image* mLoadbarloading;
	extern Image* mLogoImage;
	extern Image* mStinky;

	enum ResourceId
	{
		mLogoImage_ID,
		mLoadbar_ID,
		mLoadbarloading_ID,
		mStinky_ID,
		DEFAULTFONT_ID,
		BIGDEFAULTFONT_ID,
		DIALOGFONT_ID,
		mButtonClickSound_ID,
		mNewFishSound_ID,
		mWaterSound_ID,
		mBubblesSound_ID,
		mGlassTagSound_ID,
		mFishImage_GuBi_ID,
		mTurnFishImage_GuBi_ID,
		mBtnFishImage_Anth_ID,
		mTankImage_Rufus_ID,
		mFishImage_Presto_ID,
		mBtnFishImage_Presto_ID,
		mBtnFishImage_Blip_ID,
		mFishImage_Blip_ID,
		mWaterMaskImage_ID,
		mBubble_ID,
		mBGImage_ID,
		mLightImage1_ID,
		mLightImage2_ID,
		mTankImage_Rhubarb_ID,
		mNikoImage_ID,
		mFishsImage1_ID,
		mFishsImage2_ID,
		mRefImage1_ID,
		mRefImage2_ID,
		mCommandHeadImage_ID,
		mBtnImage_ID,
		mBtnActiveImage_ID,
		mBtnPressImage_ID,
		mBtnMaskImage_ID,
		mBtnMenuPushImage_ID,
		mBtnMenuActiveImage_ID,
		mDialogPanel_ID,
		mOptionsButton_ID,
		mCheckButton_ID,
		mUnCheckButton_ID,
		mSliderTrack_ID,
		mSliderWidget_ID,
		RESOURCE_ID_MAX
	};

	Image* GetImageById(int theId);
	Font* GetFontById(int theId);
	int GetSoundById(int theId);

	Image*& GetImageRefById(int theId);
	Font*& GetFontRefById(int theId);
	int& GetSoundRefById(int theId);

	ResourceId GetIdByImage(Image *theImage);
	ResourceId GetIdByFont(Font *theFont);
	ResourceId GetIdBySound(int theSound);
	const char* GetStringIdById(int theId);
	ResourceId GetIdByStringId(const char *theStringId);

} // namespace Sexy


#endif
