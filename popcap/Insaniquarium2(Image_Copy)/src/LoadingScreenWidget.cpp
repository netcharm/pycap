
#include "GameApp.h"
#include "LoadingScreenWidget.h"


using namespace Sexy;


LoadingScreen::LoadingScreen(Sexy::GameApp *theApp):mSCel(0),mSleep(0)
{
	anApp=theApp;
	mWidth=anApp->mWidth;
	mHeight=anApp->mHeight;

}


LoadingScreen::~LoadingScreen()
{


}

void LoadingScreen::Update()
{
	Widget::Update();



	MarkDirty();


}

void LoadingScreen::UpdateF(float theFrac)
{
    if(mSleep++==3)
    {
        if(mSCel++==mStinky->mNumCols-1)
            mSCel=0;

        mSleep=0;

    }


}

void LoadingScreen::Draw(Graphics *g)
{
	g->SetColor(Color::Black);
	g->FillRect(0,0,mWidth,mHeight);
	
	g->DrawImage(mLogoImage,0,0);

    int loaderBarWidth = mLoadbar->GetWidth();
    int loadingBarWidth=mLoadbarloading->GetWidth();

    int drawWidth = (int)(anApp->GetLoadingThreadProgress() * loaderBarWidth);
    int drawloadingWidth=(int)(anApp->GetLoadingThreadProgress() * loadingBarWidth);
    if (drawWidth > 0)
        {
        g->DrawImage(mLoadbar, mWidth / 2-16 - loaderBarWidth / 2,416,Rect(0, 0, drawWidth, mLoadbar->GetHeight()));
        g->DrawImage(mLoadbarloading, mWidth / 2-16 - loadingBarWidth / 2,416,Rect(0, 0, drawloadingWidth, mLoadbarloading->GetHeight())); 
        g->DrawImageCel(mStinky, 80+drawWidth+mStinky->GetCelWidth(), 388,mSCel);
        }


}


void LoadingScreen::AddedToManager(Sexy::WidgetManager *theWidgetManager)
{
	Widget::AddedToManager(theWidgetManager);


}

void LoadingScreen::RemovedFromManager(Sexy::WidgetManager *theWidgetManager)
{
	Widget::RemovedFromManager(theWidgetManager);


}