#ifndef __FISH__
#define __FISH__


namespace Sexy
{
	class GameApp;
	class Graphics;
	class Image;

	class Fish
	{
	private:
		bool mReptile;
		int mX;
		int mY;
		int mCel;
		int mFSpeed;
		int mSleep;
		int mAnimStep;
		int mCategory;
		bool bTurnTime;
		Image* mFishImage;
		Image* mTurnFishImage;

		Image* mLeftFishImage;
		Image* mRightFishImage;

		void SetNextLocal();
	public:
		Fish(GameApp*,Image*,Image*,int,bool=false,int=340);
		virtual ~Fish();
		//�ٶ�
		int mSpeed;
		//����
		int mFishSleep;
		//local
		int X;
		int Y;
		void Draw(Graphics*);
		void Update();

	};

}

#endif