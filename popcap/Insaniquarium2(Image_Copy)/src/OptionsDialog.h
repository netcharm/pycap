#ifndef __OPTIONSDIALOG__
#define __OPTIONSDIALOG__

#include <Dialog.h>
#include <SliderListener.h>
#include <CheckboxListener.h>

namespace Sexy
{
    #define BGMUSIC_COUNT 4
	class Graphics;
	class Slider;
	class DialogButton;
	class Checkbox;
	class GameApp;

class OptionsDialog : public Dialog, public SliderListener, public CheckboxListener
{
private:
	GameApp* anApp;
	protected:

		Slider*			mMusicVolumeSlider;
		Slider*			mSfxVolumeSlider;
		

        DialogButton*   mOKBtn;
        DialogButton*   mAboutBtn;

	public:
		enum
		{
            BUTTON_OK=201,
            BUTTON_ABOUT=202,
            MUSIC_SLIDER=300,
            SFX_SLIDER=310,
            HARDWARE_CHECKBOX=500,
            FS_CHECKBOX=510,
            BGMUSIC_CHECKBOX=600,

			DIALOG_ID=1339,
            MESSAGE_BOX=1500
		};
		Checkbox*		m3DCheckbox;			
		Checkbox*		mFSCheckbox;	
        Checkbox* mBGMusicCheckbox[BGMUSIC_COUNT];
	public:
     	OptionsDialog(GameApp* theApp,std::string theHeader, std::string theBody);
		virtual ~OptionsDialog();

		virtual void Draw(Graphics* g);
		virtual void Update();

		virtual void ButtonDepress(int theId);
		virtual void	AddedToManager(WidgetManager* theWidgetManager);
		virtual void	RemovedFromManager(WidgetManager* theWidgetManager);
		virtual void	Resize(int theX, int theY, int theWidth, int theHeight);
		virtual void	SliderVal(int theId, double theVal);
		void CheckboxChecked(int theId, bool checked);
};


}
#endif