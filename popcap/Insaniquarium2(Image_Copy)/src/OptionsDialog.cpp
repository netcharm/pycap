#include <SexyAppBase.h>
#include <WidgetManager.h>
#include <Font.h>
#include <DialogButton.h>
#include <Checkbox.h>
#include <Slider.h>


#include "OptionsDialog.h"
#include "Res.h"
#include "GameApp.h"

using namespace Sexy;

OptionsDialog::OptionsDialog(GameApp* theApp,std::string theHeader, std::string theBody)
:Dialog(mDialogPanel,NULL, OptionsDialog::DIALOG_ID, true, StringToSexyStringFast(theHeader), StringToSexyStringFast(theBody), SPACE, Dialog::BUTTONS_NONE)
{

    anApp=theApp;
    mContentInsets = Insets(23, 20, 23, 20);


    mSpaceAfterHeader = 30;



    SetHeaderFont(DIALOGFONT);
    SetLinesFont(DEFAULTFONT);
    SetButtonFont(DEFAULTFONT);

    SetColor(COLOR_HEADER, Color(253,148,4));
    SetColor(COLOR_LINES, Color::Black);

    mOKBtn=NULL;
    mAboutBtn=NULL;

    mOKBtn=new DialogButton(NULL,OptionsDialog::BUTTON_OK,this);
    mOKBtn->mButtonImage=theApp->mOptionsBtnImage;
    mOKBtn->mOverImage=theApp->mOptionsBtnAImage;
    mOKBtn->mDownImage=theApp->mOptionsBtnPImage;
    mOKBtn->SetFont(DIALOGFONT);
    mOKBtn->SetColor(ButtonWidget::COLOR_LABEL,Color(255,255,0));
    mOKBtn->SetColor(ButtonWidget::COLOR_LABEL_HILITE,Color::White);
    mOKBtn->mLabel=(_S(_T("OK")));

    mAboutBtn=new DialogButton(NULL,OptionsDialog::BUTTON_ABOUT,this);
    mAboutBtn->mButtonImage=theApp->mOptionsBtnImage;
    mAboutBtn->mOverImage=theApp->mOptionsBtnAImage;
    mAboutBtn->mDownImage=theApp->mOptionsBtnPImage;
    mAboutBtn->SetFont(DIALOGFONT);
    mAboutBtn->SetColor(ButtonWidget::COLOR_LABEL,Color::White);
    mAboutBtn->SetColor(ButtonWidget::COLOR_LABEL_HILITE,Color::White);
    mAboutBtn->mLabel=(_S(_T("My Blog")));


    mMusicVolumeSlider = new Slider(mSliderTrack, mSliderWidget, OptionsDialog::MUSIC_SLIDER, this);


    mMusicVolumeSlider->SetValue(gSexyAppBase->GetMusicVolume());

    mSfxVolumeSlider = new Slider(mSliderTrack, mSliderWidget, OptionsDialog::SFX_SLIDER, this);


    mSfxVolumeSlider->SetValue(gSexyAppBase->GetSfxVolume());





    m3DCheckbox = new Checkbox(mUnCheckButton, mCheckButton, OptionsDialog::HARDWARE_CHECKBOX, this);
    mFSCheckbox = new Checkbox(mUnCheckButton, mCheckButton, OptionsDialog::FS_CHECKBOX, this);

    for(int i=0;i<BGMUSIC_COUNT;i++)
        mBGMusicCheckbox[i]=new Checkbox(mUnCheckButton,mCheckButton,OptionsDialog::BGMUSIC_CHECKBOX+i,this);





}


OptionsDialog::~OptionsDialog()
{

    delete mMusicVolumeSlider;
    delete mSfxVolumeSlider;

    delete m3DCheckbox;
    delete mFSCheckbox;

    for(int i=0;i<BGMUSIC_COUNT;i++)
        delete mBGMusicCheckbox[i];

    delete mOKBtn;
    delete mAboutBtn;

}


void OptionsDialog::Draw(Graphics* g)
{
    Dialog::Draw(g);

    //所有UI包括Dialog的坐标都是针对GameApp的绝对坐标

    g->SetFont(DEFAULTFONT);
    g->SetColor(Color::White);


    g->DrawString(_S(_T("Music volume:")), 10+mMusicVolumeSlider->mX - mX,mMusicVolumeSlider->mY - mY - mMusicVolumeSlider->mHeight+30);

    g->DrawString(_S(_T("Sound volume:")), 10+mSfxVolumeSlider->mX - mX,mSfxVolumeSlider->mY - mY - mSfxVolumeSlider->mHeight+30);


    g->DrawString(_S(_T("3D Mode:")), m3DCheckbox->mX - mX, m3DCheckbox->mY - mY - m3DCheckbox->mHeight + 40);
    g->DrawString(_S(_T("Full Screen:")), mFSCheckbox->mX - mX, mFSCheckbox->mY - mY - mFSCheckbox->mHeight + 40);

    for(int i=0;i<BGMUSIC_COUNT;i++)
        g->DrawString(StrFormat(_S(_T("BG%d")),i+1),mBGMusicCheckbox[i]->mX-mX+6,m3DCheckbox->mY-mY+60);





}

void OptionsDialog::Update()
{
    Dialog::Update();
}

void OptionsDialog::AddedToManager(WidgetManager* theWidgetManager)
{
    Dialog::AddedToManager(theWidgetManager);

    theWidgetManager->AddWidget(mMusicVolumeSlider);
    theWidgetManager->AddWidget(mSfxVolumeSlider);


    m3DCheckbox->mChecked = gSexyAppBase->Is3DAccelerated();


    mFSCheckbox->mChecked = !gSexyAppBase->mIsWindowed;

    theWidgetManager->AddWidget(m3DCheckbox);
    theWidgetManager->AddWidget(mFSCheckbox);

    for(int i=0;i<BGMUSIC_COUNT;i++)
        theWidgetManager->AddWidget(mBGMusicCheckbox[i]);

    mBGMusicCheckbox[anApp->mCurrentMusicID]->SetChecked(true,false);

    theWidgetManager->AddWidget(mOKBtn);
    theWidgetManager->AddWidget(mAboutBtn);
}


void OptionsDialog::RemovedFromManager(WidgetManager* theWidgetManager)
{
    Dialog::RemovedFromManager(theWidgetManager);


    theWidgetManager->RemoveWidget(mMusicVolumeSlider);
    theWidgetManager->RemoveWidget(mSfxVolumeSlider);

    theWidgetManager->RemoveWidget(m3DCheckbox);
    theWidgetManager->RemoveWidget(mFSCheckbox);

    for(int i=0;i<BGMUSIC_COUNT;i++)
        theWidgetManager->RemoveWidget(mBGMusicCheckbox[i]);

    theWidgetManager->RemoveWidget(mOKBtn);
    theWidgetManager->RemoveWidget(mAboutBtn);
}


void OptionsDialog::Resize(int theX, int theY, int theWidth, int theHeight)
{
    Dialog::Resize(theX, theY, theWidth, theHeight);


    mOKBtn->Resize(theX+theWidth / 2 - mOKBtn->mButtonImage->mWidth / 2 ,theY+mHeight-mOKBtn->mButtonImage->mHeight*2,mOKBtn->mButtonImage->mWidth,mOKBtn->mButtonImage->mHeight);
    mAboutBtn->Layout(LAY_Above | LAY_SameLeft | LAY_SameTop | LAY_SameSize,mOKBtn,0,-(mOKBtn->mHeight+5));   


    mMusicVolumeSlider->Resize(theX + mContentInsets.mLeft,theY + 60,mWidth - mContentInsets.mLeft - mContentInsets.mRight,mSliderWidget->GetHeight());


    mSfxVolumeSlider->Layout(LAY_SameLeft | LAY_Below | LAY_SameSize,mMusicVolumeSlider, 0, 10, 0, 0);

    m3DCheckbox->Resize(theX + mContentInsets.mLeft+10, theY+155,mCheckButton->mWidth, mCheckButton->mHeight);


    mFSCheckbox->Layout(LAY_SameLeft| LAY_SameTop | LAY_SameSize, m3DCheckbox,120);

    for(int i=0;i<BGMUSIC_COUNT;i++)
        mBGMusicCheckbox[i]->Layout(LAY_SameLeft| LAY_SameTop | LAY_SameSize,m3DCheckbox,i*60,70);




}


void OptionsDialog::SliderVal(int theId, double theVal)
{

    if (theId == OptionsDialog::MUSIC_SLIDER)
    {

        gSexyAppBase->SetMusicVolume(theVal);
    }
    else if (theId == OptionsDialog::SFX_SLIDER)
    {

        gSexyAppBase->SetSfxVolume(theVal);


        //音效音量设置后播放试听
        if (!mSfxVolumeSlider->mDragging)
            gSexyAppBase->PlaySample(mButtonClickSound);
    }

}


void OptionsDialog::ButtonDepress(int theId)
{
    Dialog::ButtonDepress(theId);

    anApp->PlaySample(mButtonClickSound);

    for(int i=0;i<BGMUSIC_COUNT;i++)
    {
        if(mBGMusicCheckbox[i]->IsChecked())
        {
            if(anApp->mCurrentMusicID!=i)
            {
                switch(i)
                {
                case 0:
                    anApp->PlayMusic(GameApp::mBG1);
                    break;
                case 1:
                    anApp->PlayMusic(GameApp::mBG2);
                    break;
                case 2:
                    anApp->PlayMusic(GameApp::mBG3);
                    break;
                case 3:
                    anApp->PlayMusic(GameApp::mBG4);
                    break;
                default:
                    break;
                }
            }
            break;
        }

    }

    if (theId == mAboutBtn->mId)
    {
        ShellExecute(anApp->mHWnd,_T("open"),_T("http://www.RainCoding.com/blog"),NULL,NULL,SW_NORMAL);
    }
    else
        if (theId == mOKBtn->mId)
        {

            gSexyAppBase->SwitchScreenMode(!mFSCheckbox->mChecked, m3DCheckbox->mChecked);

            gSexyAppBase->KillDialog(this);
            anApp->SetFocus();
        }

}

void OptionsDialog::CheckboxChecked(int theId, bool checked)
{

    anApp->PlaySample(mButtonClickSound);
    if (theId == m3DCheckbox->mId)
    {
        if (checked)
        {
            //检测是否最低支持3D加速功能
            if (!gSexyAppBase->Is3DAccelerationSupported())
            {
                //不支持则提示
                m3DCheckbox->SetChecked(false);
                gSexyAppBase->DoDialog(OptionsDialog::MESSAGE_BOX, true, _S(_T("Not Supported")), 
                    _S(_T("Hardware acceleration can not be enabled on this computer. \nYour video card does not meet the minimum requirements for this game.")),
                       _S(_T("OK")), Dialog::BUTTONS_FOOTER);
            }
            //是否能完全支持SexyFramework的所有加速功能
            else if(gSexyAppBase->Is3DAccelerationRecommended())
            {
                gSexyAppBase->DoDialog(OptionsDialog::MESSAGE_BOX, true, _S(_T("Warning")), 
                    _S(_T("Your video card may not fully support this feature.\nIf you experience slower performance, please disable Hardware Acceleration.")),
                       _S(_T("OK")), Dialog::BUTTONS_FOOTER);
            }

        }
    }
    else if (theId == mFSCheckbox->mId)
    {
        if (gSexyAppBase->mForceFullscreen && !checked)
        {
            gSexyAppBase->DoDialog(OptionsDialog::MESSAGE_BOX, true, _S(_T("No Windowed Mode")),
                _S(_T("Windowed mode is only available if your desktop is running in\neither 16 bit or 32 bit color mode, which it is not.")), _S(_T("OK")), Dialog::BUTTONS_FOOTER);

            mFSCheckbox->SetChecked(true);
        }
    }
    else 
    {
        for(int i=0;i<BGMUSIC_COUNT;i++)
        {
            mBGMusicCheckbox[i]->SetChecked((theId==mBGMusicCheckbox[i]->mId),false);

        }

    }

}