#ifndef __GAMEAPP__
#define __GAMEAPP__

#include <tchar.h>
#include <DDImage.h>
#include <SexyAppBase.h>
#include <BassMusicInterface.h>
#include <WidgetManager.h>
#include <ImageFont.h>

#include "Res.h"




namespace Sexy
{
    //请保留Title信息已作对我劳动最基本的尊重^_^
    #define SPACE _S(_T(""))
#define Title_Caption _S(_T("怪怪水族馆图像演示(Ver:0.1)-\"RainBlog:http://www.RainCoding.com/blog\""));
    #define Pause_Text _S(_T("PAUSE"))
    #define Proc_Name _T("Popcap Framework 教程5")
    #define Proc_Ver _T("0.1")
    #define Game_Width 640
    #define Game_Height 480
    #define Game_Rect_Height Game_Height-180

    class MainWidget;
    class WidgetManager;
    class LoadingScreen;

    class GameApp:public SexyAppBase
	{
	private:
          MainWidget* mMainWidget;
		  LoadingScreen* mLoadingScreen;
	public:
        int mCurrentMusicID;
		enum {mBackGroundMusic};//,mButtonClickSound,mNewFishSound,mWaterSound,mGlassTagSound,mBubblesSound};
		enum {mBG1=0,mBG2=12,mBG3=25,mBG4=58,mBGLoading=37};
		enum {mOptionDialogId=1339};


		Image* mTurnTankImage_Rhubarb;

		

		Image* mTurnTankImage_Rufus;
		Image* mBtnFishImage_GuBi;
	

		Image* mTurnFishImage_Presto;

		Image* mTurnFishImage_Blip;
        Image* mOptionsBtnImage;
        Image* mOptionsBtnAImage;
        Image* mOptionsBtnPImage;
		

		GameApp();
		virtual ~GameApp();

		virtual void Init();
		virtual void LoadingThreadProc();
		virtual void LoadingThreadCompleted();
		virtual void LostFocus();
		virtual void GotFocus();
		void SetFocus();
      
        //重载创建Dialog函数
        virtual Dialog* NewDialog(int theDialogId, bool isModal, const std::string& theDialogHeader,const std::string& theDialogLines, const std::string& theDialogFooter, int theButtonMode);

        //Dialog按钮事件重载
        virtual void ButtonDepress(int theId);

        //重载模式切换
   		virtual void SwitchScreenMode(bool wantWindowed, bool is3d);

        void PlayMusic(int theOffset);

	};

}

#endif