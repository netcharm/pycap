#include <SoundManager.h>
#include <ResourceManager.h>
#include <Dialog.h>
#include <DialogButton.h>
#include <Checkbox.h>

#include "OptionsDialog.h"
#include "GameApp.h"
#include "MainWidget.h"
#include "LoadingScreenWidget.h"

using namespace Sexy;

GameApp::GameApp()
{
	mProdName = Proc_Name;
	mProductVersion = Proc_Ver;
	mTitle=Title_Caption;
	mWidth=Game_Width;
	mHeight=Game_Height;

	//允许调用UpdateF
	mVSyncUpdates = true;
	//开启同步垂直等待
	mWaitForVSync = true;
	//自动3D加速
	mAutoEnable3D = true;

    




	mMainWidget=NULL;
	mLoadingScreen=NULL;

  mTurnTankImage_Rhubarb=mTurnTankImage_Rufus=mBtnFishImage_GuBi=\
  mTurnFishImage_Presto=mTurnFishImage_Blip=mOptionsBtnImage=\
  mOptionsBtnAImage=mOptionsBtnPImage=NULL;

}

GameApp::~GameApp()
{
	mWidgetManager->RemoveWidget(mMainWidget);

	mSoundManager->ReleaseSounds();
	mMusicInterface->StopMusic(0);
	mMusicInterface->UnloadMusic(0);

    //删除全部资源
	mResourceManager->DeleteResources("");

	delete mLoadingScreen;
	delete mMainWidget;


	delete mTurnTankImage_Rhubarb;
	delete mTurnFishImage_Presto;

	delete mTurnTankImage_Rufus;
	delete mTurnFishImage_Blip;

	delete mBtnFishImage_GuBi;
	
    delete mOptionsBtnImage;
    delete mOptionsBtnAImage;
    delete mOptionsBtnPImage;
}

Dialog* GameApp::NewDialog(int theDialogId, bool isModal, const std::string& theDialogHeader,const std::string& theDialogLines, const std::string& theDialogFooter, int theButtonMode)
{
    Dialog* d = new Dialog(mDialogPanel,mOptionsBtnImage, theDialogId, isModal,StringToSexyStringFast(theDialogHeader), StringToSexyStringFast(theDialogLines), StringToSexyStringFast(theDialogFooter), theButtonMode);

    d->mContentInsets = Insets(23, 20, 23, mOptionsBtnImage->mHeight);

    d->mSpaceAfterHeader = 30;



    d->SetHeaderFont(DIALOGFONT);
    d->SetLinesFont(DEFAULTFONT);
    d->SetButtonFont(DEFAULTFONT);

    d->SetColor(Dialog::COLOR_HEADER, Color(253,148,4));
    d->SetColor(Dialog::COLOR_LINES, Color::White);
    d->SetColor(Dialog::COLOR_BUTTON_TEXT,Color(255,255,0));
    d->SetColor(Dialog::COLOR_BUTTON_TEXT_HILITE,Color::White);

    d->mSpaceAfterHeader = 30;
    d->Resize(100, 100, 300, 250);

    return d;
}

void GameApp::ButtonDepress(int theId)
{
    PlaySample(mButtonClickSound);
    //ID_YES=Dialog_ID+2000
    if (theId == OptionsDialog::MESSAGE_BOX+2000)
        KillDialog(OptionsDialog::MESSAGE_BOX);
}

void GameApp::SwitchScreenMode(bool wantWindowed, bool is3d)
{
    SexyAppBase::SwitchScreenMode(wantWindowed, is3d);

    //获取配置Dialog
    OptionsDialog* d = (OptionsDialog *) GetDialog(OptionsDialog::DIALOG_ID);


    //如果是全屏模式则恢复到窗口模式
    if ((d) && (d->mFSCheckbox))
        d->mFSCheckbox->SetChecked(!wantWindowed);

}

void GameApp::PlayMusic(int theOffset)
{
    if(this->mMusicInterface)
        this->mMusicInterface->PlayMusic(GameApp::mBackGroundMusic,theOffset);

        switch(theOffset)
        {
        case mBG1:
            mCurrentMusicID=0;
            break;
        case mBG2:
            mCurrentMusicID=1;
            break;

        case mBG3:
            mCurrentMusicID=2;
            break;

        case mBG4:
            mCurrentMusicID=3;
            break;

        default:
            break;
        }
}


void GameApp::LoadingThreadProc()
{

    //采用xml方式载入资源,免去手工载入的烦琐
	mResourceManager->StartLoadResources("Game");

	while (mResourceManager->LoadNextResource())
	{

        //望文生意
		mCompletedLoadingThreadTasks++;

		if (mShutdown)
			return;

		mLoadingScreen->MarkDirty();
	}


    //有资源载入失败
	if (mResourceManager->HadError() || !ExtractGameResources(mResourceManager))
	{		
		ShowResourceError(false);
		mLoadingFailed = true;

		return;
	}



    //容错一下
    mOptionsBtnImage=CopyImage(mOptionsButton,Rect(mOptionsButton->GetCelWidth(),0,mOptionsButton->GetCelWidth(),mOptionsButton->GetCelHeight()));
    if(mOptionsBtnImage)
    {
        mOptionsBtnAImage=CopyImage(mOptionsButton,Rect(0,0,mOptionsButton->GetCelWidth(),mOptionsButton->GetCelHeight()));
        if(mOptionsBtnAImage)
        {
            mOptionsBtnPImage=CopyImage(mOptionsButton,Rect(2*mOptionsButton->GetCelWidth(),0,mOptionsButton->GetCelWidth(),mOptionsButton->GetHeight()));
            if (mOptionsBtnPImage)
            {
                mBtnFishImage_GuBi=CopyImage(mFishImage_GuBi,Rect(0,2*mFishImage_GuBi->GetCelHeight(),mFishImage_GuBi->mWidth,mFishImage_GuBi->GetCelHeight()));
                if(mBtnFishImage_GuBi)
                {
                    mBtnFishImage_GuBi->mNumCols=10;
                    mBtnFishImage_GuBi->mNumRows=1;

                    mTurnTankImage_Rufus=CopyImage(mTankImage_Rufus,Rect(0,mTankImage_Rufus->GetCelHeight(),mTankImage_Rufus->mWidth,mTankImage_Rufus->GetCelHeight()));
                    if(mTurnTankImage_Rufus)
                    {
                        mTurnTankImage_Rufus->mNumCols=10;
                        mTurnTankImage_Rufus->mNumRows=1;

                        mTurnFishImage_Presto=CopyImage(mFishImage_Presto,Rect(0,mFishImage_Presto->GetCelHeight(),mFishImage_Presto->mWidth,mFishImage_Presto->GetCelHeight()));
                        if(mTurnFishImage_Presto)
                        {
                            mTurnFishImage_Presto->mNumCols=10;
                            mTurnFishImage_Presto->mNumRows=1;

                            mTurnFishImage_Blip=CopyImage(mFishImage_Blip,Rect(0,mFishImage_Blip->GetCelHeight(),mFishImage_Blip->mWidth,mFishImage_Blip->GetCelHeight()));
                            if(mTurnFishImage_Blip)
                            {
                                mTurnFishImage_Blip->mNumCols=10;
                                mTurnFishImage_Blip->mNumRows=1;

                                mTurnTankImage_Rhubarb=CopyImage(mTankImage_Rhubarb,Rect(0,mTankImage_Rhubarb->GetCelHeight(),mTankImage_Rhubarb->mWidth,mTankImage_Rhubarb->GetCelHeight()));
                                if(mTurnTankImage_Rhubarb)
                                {
                                    mTurnTankImage_Rhubarb->mNumCols=10;
                                    mTurnTankImage_Rhubarb->mNumRows=1;
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }


        Popup(_T("图像处理发生错误!"));
        mLoadingFailed=true;
}

void GameApp::Init()
{
	SexyAppBase::Init();

	mMusicInterface->LoadMusic(mBackGroundMusic, "sounds/Insaniq2.mo3");
    PlayMusic(GameApp::mBGLoading);

	LoadResourceManifest();

	if (!mResourceManager->LoadResources("Init"))
	{
		ShowResourceError(true);
		mLoadingFailed = true;
		return;
	}

	if (!ExtractInitResources(mResourceManager))
	{
		ShowResourceError(true);
		mLoadingFailed = true;
		return;
	}

	mLoadingScreen=new LoadingScreen(this);
	mWidgetManager->AddWidget(mLoadingScreen);

    //Total res count
	mNumLoadingThreadTasks = mResourceManager->GetNumResources("Game");

}

void GameApp::LoadingThreadCompleted()
{
	SexyAppBase::LoadingThreadCompleted();

	mWidgetManager->RemoveWidget(mLoadingScreen);
	delete mLoadingScreen;
	mLoadingScreen=NULL;
	mResourceManager->DeleteResources("Init");
    mMusicInterface->StopMusic(mBackGroundMusic);
	if (mLoadingFailed)
		return;

	mMainWidget=new MainWidget(this);
	mWidgetManager->AddWidget(mMainWidget);
	mWidgetManager->SetFocus(mMainWidget);
}

void GameApp::LostFocus()
{
	SexyAppBase::LostFocus();

	if (mMainWidget)
		mMainWidget->Pause(true);
}

void GameApp::GotFocus()
{
	SexyAppBase::GotFocus();

	if (mMainWidget)
		mMainWidget->Pause(false);
}

void GameApp::SetFocus()
{
	if(mMainWidget)
		mWidgetManager->SetFocus(mMainWidget);
}




