#include "Res.h"
#include <ResourceManager.h>

using namespace Sexy;

#pragma warning(disable:4311 4312)

static bool gNeedRecalcVariableToIdMap = false;

bool Sexy::ExtractResourcesByName(ResourceManager *theManager, const char *theName)
{
	if (strcmp(theName,"Game")==0) return ExtractGameResources(theManager);
	if (strcmp(theName,"Init")==0) return ExtractInitResources(theManager);
	return false;
}

Sexy::ResourceId Sexy::GetIdByStringId(const char *theStringId)
{
	typedef std::map<std::string,int> MyMap;
	static MyMap aMap;
	if(aMap.empty())
	{
		for(int i=0; i<RESOURCE_ID_MAX; i++)
			aMap[GetStringIdById(i)] = i;
	}

	MyMap::iterator anItr = aMap.find(theStringId);
	if (anItr == aMap.end())
		return RESOURCE_ID_MAX;
	else
		return (ResourceId) anItr->second;
}

// Game Resources
Font* Sexy::BIGDEFAULTFONT;
Font* Sexy::DEFAULTFONT;
Font* Sexy::DIALOGFONT;
Image* Sexy::mBGImage;
Image* Sexy::mBtnActiveImage;
Image* Sexy::mBtnFishImage_Anth;
Image* Sexy::mBtnFishImage_Blip;
Image* Sexy::mBtnFishImage_Presto;
Image* Sexy::mBtnImage;
Image* Sexy::mBtnMaskImage;
Image* Sexy::mBtnMenuActiveImage;
Image* Sexy::mBtnMenuPushImage;
Image* Sexy::mBtnPressImage;
Image* Sexy::mBubble;
int Sexy::mBubblesSound;
int Sexy::mButtonClickSound;
Image* Sexy::mCheckButton;
Image* Sexy::mCommandHeadImage;
Image* Sexy::mDialogPanel;
Image* Sexy::mFishImage_Blip;
Image* Sexy::mFishImage_GuBi;
Image* Sexy::mFishImage_Presto;
Image* Sexy::mFishsImage1;
Image* Sexy::mFishsImage2;
int Sexy::mGlassTagSound;
Image* Sexy::mLightImage1;
Image* Sexy::mLightImage2;
int Sexy::mNewFishSound;
Image* Sexy::mNikoImage;
Image* Sexy::mOptionsButton;
Image* Sexy::mRefImage1;
Image* Sexy::mRefImage2;
Image* Sexy::mSliderTrack;
Image* Sexy::mSliderWidget;
Image* Sexy::mTankImage_Rhubarb;
Image* Sexy::mTankImage_Rufus;
Image* Sexy::mTurnFishImage_GuBi;
Image* Sexy::mUnCheckButton;
Image* Sexy::mWaterMaskImage;
int Sexy::mWaterSound;

bool Sexy::ExtractGameResources(ResourceManager *theManager)
{
	gNeedRecalcVariableToIdMap = true;

	ResourceManager &aMgr = *theManager;
	try
	{
		BIGDEFAULTFONT = aMgr.GetFontThrow("BIGDEFAULTFONT");
		DEFAULTFONT = aMgr.GetFontThrow("DEFAULTFONT");
		DIALOGFONT = aMgr.GetFontThrow("DIALOGFONT");
		mBGImage = aMgr.GetImageThrow("mBGImage");
		mBtnActiveImage = aMgr.GetImageThrow("mBtnActiveImage");
		mBtnFishImage_Anth = aMgr.GetImageThrow("mBtnFishImage_Anth");
		mBtnFishImage_Blip = aMgr.GetImageThrow("mBtnFishImage_Blip");
		mBtnFishImage_Presto = aMgr.GetImageThrow("mBtnFishImage_Presto");
		mBtnImage = aMgr.GetImageThrow("mBtnImage");
		mBtnMaskImage = aMgr.GetImageThrow("mBtnMaskImage");
		mBtnMenuActiveImage = aMgr.GetImageThrow("mBtnMenuActiveImage");
		mBtnMenuPushImage = aMgr.GetImageThrow("mBtnMenuPushImage");
		mBtnPressImage = aMgr.GetImageThrow("mBtnPressImage");
		mBubble = aMgr.GetImageThrow("mBubble");
		mBubblesSound = aMgr.GetSoundThrow("mBubblesSound");
		mButtonClickSound = aMgr.GetSoundThrow("mButtonClickSound");
		mCheckButton = aMgr.GetImageThrow("mCheckButton");
		mCommandHeadImage = aMgr.GetImageThrow("mCommandHeadImage");
		mDialogPanel = aMgr.GetImageThrow("mDialogPanel");
		mFishImage_Blip = aMgr.GetImageThrow("mFishImage_Blip");
		mFishImage_GuBi = aMgr.GetImageThrow("mFishImage_GuBi");
		mFishImage_Presto = aMgr.GetImageThrow("mFishImage_Presto");
		mFishsImage1 = aMgr.GetImageThrow("mFishsImage1");
		mFishsImage2 = aMgr.GetImageThrow("mFishsImage2");
		mGlassTagSound = aMgr.GetSoundThrow("mGlassTagSound");
		mLightImage1 = aMgr.GetImageThrow("mLightImage1");
		mLightImage2 = aMgr.GetImageThrow("mLightImage2");
		mNewFishSound = aMgr.GetSoundThrow("mNewFishSound");
		mNikoImage = aMgr.GetImageThrow("mNikoImage");
		mOptionsButton = aMgr.GetImageThrow("mOptionsButton");
		mRefImage1 = aMgr.GetImageThrow("mRefImage1");
		mRefImage2 = aMgr.GetImageThrow("mRefImage2");
		mSliderTrack = aMgr.GetImageThrow("mSliderTrack");
		mSliderWidget = aMgr.GetImageThrow("mSliderWidget");
		mTankImage_Rhubarb = aMgr.GetImageThrow("mTankImage_Rhubarb");
		mTankImage_Rufus = aMgr.GetImageThrow("mTankImage_Rufus");
		mTurnFishImage_GuBi = aMgr.GetImageThrow("mTurnFishImage_GuBi");
		mUnCheckButton = aMgr.GetImageThrow("mUnCheckButton");
		mWaterMaskImage = aMgr.GetImageThrow("mWaterMaskImage");
		mWaterSound = aMgr.GetSoundThrow("mWaterSound");
	}
	catch(ResourceManagerException&)
	{
		return false;
	}
	return true;
}

// Init Resources
Image* Sexy::mLoadbar;
Image* Sexy::mLoadbarloading;
Image* Sexy::mLogoImage;
Image* Sexy::mStinky;

bool Sexy::ExtractInitResources(ResourceManager *theManager)
{
	gNeedRecalcVariableToIdMap = true;

	ResourceManager &aMgr = *theManager;
	try
	{
		mLoadbar = aMgr.GetImageThrow("mLoadbar");
		mLoadbarloading = aMgr.GetImageThrow("mLoadbarloading");
		mLogoImage = aMgr.GetImageThrow("mLogoImage");
		mStinky = aMgr.GetImageThrow("mStinky");
	}
	catch(ResourceManagerException&)
	{
		return false;
	}
	return true;
}

static void* gResources[] =
{
	&mLogoImage,
	&mLoadbar,
	&mLoadbarloading,
	&mStinky,
	&DEFAULTFONT,
	&BIGDEFAULTFONT,
	&DIALOGFONT,
	&mButtonClickSound,
	&mNewFishSound,
	&mWaterSound,
	&mBubblesSound,
	&mGlassTagSound,
	&mFishImage_GuBi,
	&mTurnFishImage_GuBi,
	&mBtnFishImage_Anth,
	&mTankImage_Rufus,
	&mFishImage_Presto,
	&mBtnFishImage_Presto,
	&mBtnFishImage_Blip,
	&mFishImage_Blip,
	&mWaterMaskImage,
	&mBubble,
	&mBGImage,
	&mLightImage1,
	&mLightImage2,
	&mTankImage_Rhubarb,
	&mNikoImage,
	&mFishsImage1,
	&mFishsImage2,
	&mRefImage1,
	&mRefImage2,
	&mCommandHeadImage,
	&mBtnImage,
	&mBtnActiveImage,
	&mBtnPressImage,
	&mBtnMaskImage,
	&mBtnMenuPushImage,
	&mBtnMenuActiveImage,
	&mDialogPanel,
	&mOptionsButton,
	&mCheckButton,
	&mUnCheckButton,
	&mSliderTrack,
	&mSliderWidget,
	NULL
};

Image* Sexy::LoadImageById(ResourceManager *theManager, int theId)
{
	return (*((Image**)gResources[theId]) = theManager->LoadImage(GetStringIdById(theId)));
}

void Sexy::ReplaceImageById(ResourceManager *theManager, int theId, Image *theImage)
{
	theManager->ReplaceImage(GetStringIdById(theId),theImage);
	*(Image**)gResources[theId] = theImage;
}

Image* Sexy::GetImageById(int theId)
{
	return *(Image**)gResources[theId];
}

Font* Sexy::GetFontById(int theId)
{
	return *(Font**)gResources[theId];
}

int Sexy::GetSoundById(int theId)
{
	return *(int*)gResources[theId];
}

Image*& Sexy::GetImageRefById(int theId)
{
	return *(Image**)gResources[theId];
}

Font*& Sexy::GetFontRefById(int theId)
{
	return *(Font**)gResources[theId];
}

int& Sexy::GetSoundRefById(int theId)
{
	return *(int*)gResources[theId];
}

static Sexy::ResourceId GetIdByVariable(const void *theVariable)
{
	typedef std::map<int,int> MyMap;
	static MyMap aMap;
	if(gNeedRecalcVariableToIdMap)
	{
		gNeedRecalcVariableToIdMap = false;
		aMap.clear();
		for(int i=0; i<RESOURCE_ID_MAX; i++)
			aMap[*(int*)gResources[i]] = i;
	}

	MyMap::iterator anItr = aMap.find((int)theVariable);
	if (anItr == aMap.end())
		return RESOURCE_ID_MAX;
	else
		return (ResourceId) anItr->second;
}

Sexy::ResourceId Sexy::GetIdByImage(Image *theImage)
{
	return GetIdByVariable(theImage);
}

Sexy::ResourceId Sexy::GetIdByFont(Font *theFont)
{
	return GetIdByVariable(theFont);
}

Sexy::ResourceId Sexy::GetIdBySound(int theSound)
{
	return GetIdByVariable((void*)theSound);
}

const char* Sexy::GetStringIdById(int theId)
{
	switch(theId)
	{
		case mLogoImage_ID: return "mLogoImage";
		case mLoadbar_ID: return "mLoadbar";
		case mLoadbarloading_ID: return "mLoadbarloading";
		case mStinky_ID: return "mStinky";
		case DEFAULTFONT_ID: return "DEFAULTFONT";
		case BIGDEFAULTFONT_ID: return "BIGDEFAULTFONT";
		case DIALOGFONT_ID: return "DIALOGFONT";
		case mButtonClickSound_ID: return "mButtonClickSound";
		case mNewFishSound_ID: return "mNewFishSound";
		case mWaterSound_ID: return "mWaterSound";
		case mBubblesSound_ID: return "mBubblesSound";
		case mGlassTagSound_ID: return "mGlassTagSound";
		case mFishImage_GuBi_ID: return "mFishImage_GuBi";
		case mTurnFishImage_GuBi_ID: return "mTurnFishImage_GuBi";
		case mBtnFishImage_Anth_ID: return "mBtnFishImage_Anth";
		case mTankImage_Rufus_ID: return "mTankImage_Rufus";
		case mFishImage_Presto_ID: return "mFishImage_Presto";
		case mBtnFishImage_Presto_ID: return "mBtnFishImage_Presto";
		case mBtnFishImage_Blip_ID: return "mBtnFishImage_Blip";
		case mFishImage_Blip_ID: return "mFishImage_Blip";
		case mWaterMaskImage_ID: return "mWaterMaskImage";
		case mBubble_ID: return "mBubble";
		case mBGImage_ID: return "mBGImage";
		case mLightImage1_ID: return "mLightImage1";
		case mLightImage2_ID: return "mLightImage2";
		case mTankImage_Rhubarb_ID: return "mTankImage_Rhubarb";
		case mNikoImage_ID: return "mNikoImage";
		case mFishsImage1_ID: return "mFishsImage1";
		case mFishsImage2_ID: return "mFishsImage2";
		case mRefImage1_ID: return "mRefImage1";
		case mRefImage2_ID: return "mRefImage2";
		case mCommandHeadImage_ID: return "mCommandHeadImage";
		case mBtnImage_ID: return "mBtnImage";
		case mBtnActiveImage_ID: return "mBtnActiveImage";
		case mBtnPressImage_ID: return "mBtnPressImage";
		case mBtnMaskImage_ID: return "mBtnMaskImage";
		case mBtnMenuPushImage_ID: return "mBtnMenuPushImage";
		case mBtnMenuActiveImage_ID: return "mBtnMenuActiveImage";
		case mDialogPanel_ID: return "mDialogPanel";
		case mOptionsButton_ID: return "mOptionsButton";
		case mCheckButton_ID: return "mCheckButton";
		case mUnCheckButton_ID: return "mUnCheckButton";
		case mSliderTrack_ID: return "mSliderTrack";
		case mSliderWidget_ID: return "mSliderWidget";
		default: return "";
	}
}

