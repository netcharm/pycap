#include <Graphics.h>
#include <time.h>

#include "GameApp.h"
#include "Fish.h"


using namespace Sexy;



Fish::Fish(GameApp* theApp,Image* theImage,Image* theTurnImage,int Category,bool Reptile,int RY):mCel(0),mFSpeed(8),mSleep(0),mSpeed(2),
mFishSleep(10),bTurnTime(false),Y(52)
{
	mLeftFishImage=theImage;
	mRightFishImage=(DDImage *)theApp->CopyImage(mLeftFishImage);
	theApp->MirrorImage(mRightFishImage);
	mFishImage=mLeftFishImage;

	mTurnFishImage=theTurnImage;

	mReptile=Reptile;
	srand((int)time(NULL));  
	mCategory=Category;
	X=rand()%(Game_Width-theImage->GetCelWidth()*2)+theImage->GetCelWidth();
	mX=X+mFSpeed*2;
	if(!Reptile)
		mY=100;
	else
		mY=RY;
	
}

Fish::~Fish()
{
   delete mRightFishImage;

}

void Fish::Draw(Graphics *g)
{
	if((mFSpeed!=1)&&(!mReptile))
	{
		g->SetColor(Color(255,255,255,mCel*25));
		g->SetColorizeImages(true);
	}
	else
		g->SetColorizeImages(false);

	if(!bTurnTime)
		g->DrawImageCel(mFishImage,X,Y,mCel,mCategory);
	else
	{
		if(mFishImage!=mRightFishImage)
			mAnimStep=mTurnFishImage->mNumCols-1;
		else
			mAnimStep=0;

		g->DrawImageCel(mTurnFishImage,X,Y,abs(mAnimStep-mCel),mCategory);
		bTurnTime=(mCel!=mTurnFishImage->mNumCols-1);
	}

}

void Fish::Update()
{
	if(mSleep++==mSpeed)
	{
		if(mFishImage->mNumCols-1==mCel++)
		{
			mCel=0;
		}

		if((X==mX)&&(Y==mY))
		{
			mFSpeed=1;
			if(rand()%mFishSleep==mFishSleep-1)
				SetNextLocal();
		}
		else
		{

			if(X!=mX)
				if (X>mX)
				{
					if(!bTurnTime)
					{
						bTurnTime=(mFishImage!=mLeftFishImage);
						if(bTurnTime)
							mCel=0;
					}
					mFishImage=mLeftFishImage;
					X-=mFSpeed;
				}
				else
				{
					if(!bTurnTime)
					{
						bTurnTime=(mFishImage!=mRightFishImage);
						if(bTurnTime)
							mCel=0;
					}
					mFishImage=mRightFishImage;
					X+=mFSpeed;
				}


				if(Y!=mY)
					if(Y>mY)
						Y-=mFSpeed;
					else
						Y+=mFSpeed;


		}
		mSleep=0;
	}



}

void Fish::SetNextLocal()
{
	mX=rand()%(Game_Width-mFishImage->GetCelWidth());
	if(!mReptile)
		mY=rand()%(Game_Rect_Height-mFishImage->GetCelHeight())+100;
}


