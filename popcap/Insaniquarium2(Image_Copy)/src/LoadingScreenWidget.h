#ifndef __LOADINGSCREEN__
#define __LOADINGSCREEN__

#include <Widget.h>

namespace Sexy
{
	class Graphics;
	class GameApp;
	class WidgetManager;

	class LoadingScreen:public Widget
	{
	private:
		GameApp* anApp;
        int mSCel,mSleep;

	public:
		LoadingScreen(GameApp* theApp);
		virtual ~LoadingScreen();

		virtual void Update();
        virtual void UpdateF(float theFrac);
		virtual void Draw(Graphics* g);

		virtual void AddedToManager(WidgetManager* theWidgetManager);
		virtual void RemovedFromManager(WidgetManager* theWidgetManager);

	};

}













#endif