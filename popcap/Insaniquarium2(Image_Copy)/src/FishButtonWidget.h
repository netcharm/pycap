#ifndef __FISHBUTTONWIDGET__
#define __FISHBUTTONWIDGET__

#include <ButtonWidget.h>
#include <ButtonListener.h>



namespace Sexy
{

	class GameApp;
	class Graphics;
	class Image;

	class FishButtonWidget:public ButtonWidget
	{
	private:
		int mSleep,mCel;
		bool mPause;
	public:
		Image* mBtnImage;
		Image* mMaskBtnImage;
		FishButtonWidget(int theId, ButtonListener* theButtonListener);
		virtual ~FishButtonWidget();
		virtual void Draw(Graphics* g);
		virtual void Update();	
		virtual void UpdateF(float theFrac);
		void Pause(bool p);



	};




}

#endif
